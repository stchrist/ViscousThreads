set xrange [-1:1]
set yrange [-1:1]
set zrange [-1:2.5]
set view equal xyz
set ticslevel 0

set palette model RGB defined ( 0 'blue', 1 'red' )

unset key
unset colorbox

list = system('ls output/thread_*')
do for [file in list] {
#    splot file using 1:2:3:($3 < 0 ? 1 : 0) with points pointtype 7 ps 1 palette
    splot file using 1:2:3 with points pointtype 7
    pause 0.5
}
