import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize=(10,1))
ax = fig.add_subplot(111)

x = np.linspace(0.5, 20, 200)
y1 = np.sin(x)
y2 = np.zeros(len(x))

intersections = [np.pi, 2*np.pi, 3*np.pi, 4*np.pi, 5*np.pi, 6*np.pi]

plt.fill_between(x, y1, color='r', alpha=0.2,edgecolor="b", linewidth=0.0)
ax.plot(x,y1)
ax.plot(x,y2, color='g')

ax.scatter(intersections, np.zeros(len(intersections)), c='r', zorder=100)

plt.axis('off')

plt.savefig('reward-difference-illustration.pdf', bbox_inches='tight')
plt.show()
