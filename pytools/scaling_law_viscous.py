import sys
import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]
data = np.loadtxt(filename)

dt = data[:,0]
Q = data[:,1]
H = data[:,2]
r = data[:,3]
freq = data[:,4]


fig, (ax1) = plt.subplots(1,1)
#fig.tight_layout()
#fig.subplots_adjust(left=0.1, top=0.9, wspace=0.5)

xs = r
xs = np.append(xs, max(r)*1.1)
xs = np.append(xs, min(r)/1.1)

ax1.loglog(r, freq * H / Q, 's')
ax1.loglog(xs, pow(xs, -2) * 0.6)


#Fit line to curve in first plot
b = freq * H / Q
coefficients = np.polyfit(np.log10(r), np.log10(b), 1)

ys = pow(10, coefficients[1]) * pow(xs, coefficients[0])
ax1.loglog(xs, ys, 'r')
#print b
#print ys
print coefficients[0], coefficients[1]

ax1.set_title("Viscous coiling \n(slope should be -2)")
ax1.set_xlabel(r"$r$")
ax1.set_ylabel(r"$\Omega H / Q$")


#ax2.loglog(r, freq / pow(Q, 0.75), 's')
#ax2.loglog(r, pow(r, -2) * 0.05)
#ax2.set_title("Gravitational coiling \n(slope should be -2)")
#ax2.set_xlabel(r"$r$")
#ax2.set_ylabel(r"$\Omega / Q^{0.75}$")

#ax3.loglog(r, freq / pow(Q, 4.0/3.0), 's')
#ax3.loglog(r, pow(r, -10/3) * 0.001)

#for i, txt in enumerate(H):
#    ax3.text(r[i], pow(Q[i], 4.0/3.0), txt) #, transform=ax3.transAxes)

#ax3.set_title("Intertial coiling \n(slope should be -10/3)")
#ax3.set_xlabel(r"$r$")
#ax3.set_ylabel(r"$\Omega / Q^{1.33}$")

plt.savefig(filename[:-4] + '_viscous.png')
plt.show()
