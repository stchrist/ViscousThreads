import numpy as np
import matplotlib.pyplot as plt
from sys import argv

filename = argv[1]
data = np.loadtxt(filename, delimiter=',')

plt.plot(data[::2], data[1::2])
#plt.scatter(data[::2], data[1::2], color='r')

plt.axis('off')
plt.axes().set_aspect('equal')

plt.savefig(filename[:-6] + '.png', bbox_inches='tight')
plt.show()
