import sys
import numpy as np
import matplotlib.pyplot as plt
import os.path
from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition,
                                                          mark_inset)

filename = sys.argv[1]
data = np.loadtxt(filename)

filename_stabilized = filename[:-10] + '_stabilized.dat'
data_stabilized = []
if os.path.exists(filename_stabilized):
    data_stabilized = np.loadtxt(filename_stabilized)


colorgradient = False
#colorgradient = True
#inset = False
inset = True

startidx = 0
#startidx = 14500

if len(data_stabilized) > 0:
    startidx = len(data_stabilized) - 1 # start at last point in stabilized pattern

x = data[startidx:,0]
y = data[startidx:,1]

fig, ax = plt.subplots()
ax.axhline(0, color='black')
ax.axvline(0, color='black')

if inset:
#    ax2 = plt.axes([0.68, 0.65, 0.2, 0.2])

    # Create a set of inset Axes: these should fill the bounding box allocated to   
    # them.
    ax2 = plt.axes([0,0,1,1])
    # Manually set the position and relative size of the inset axes within ax1
    ip = InsetPosition(ax, [0.45,0.45,0.5,0.5])
    ax2.set_axes_locator(ip)
    # Mark the region corresponding to the inset axes on ax1 and draw lines
    # in grey linking the two axes.
    mark_inset(ax, ax2, loc1=3, loc2=4, fc="none", ec='0.5')
    ax2.axhline(0, color='black')



if not colorgradient:
    ax.plot(x, y)
    if inset:
        ax2.plot(x, y)
    if len(data_stabilized) > 0:
        ax.plot(data_stabilized[:,0], data_stabilized[:,1])
        ax.scatter(data_stabilized[-1,0], data_stabilized[-1,1], s=20, color='r', zorder=100)
        if inset:
            minx = 21.1 # minimum value on x axis (should be within stabilized range)
            ind = np.where(data_stabilized[:,0] > 21.1)
            ax2.plot(data_stabilized[ind[0],0], data_stabilized[ind[0],1])
            ax2.scatter(data_stabilized[-1,0], data_stabilized[-1,1], s=20, color='r', zorder=100)
else:
    N = len(y)
    for i in range(N-1):
        ax.plot(x[i:i+2], y[i:i+2], color=plt.cm.jet(255*i/(N-1)))

#if inset:
#    plt.setp(ax2, ylim=[-4e-5, 4e-5])


# Set aspect ratio
#ax.set_aspect('equal')


# Set axis label and location
ax.set_xlabel('x')
#ax.xaxis.set_label_coords(0.96,0.15)
ax.set_ylabel('y', rotation=0)
#ax.yaxis.set_label_coords(0.05,0.87)

if inset:
    ax2.ticklabel_format(style='sci', scilimits=(-2,2))
#    ax2.set_xticklabels(ax2.get_xticks(), backgroundcolor='w')
#    ax2.set_yticklabels(ax2.get_yticks(), backgroundcolor='w')


# Set axis ticks
#ax.minorticks_off()
#ax.set_xticks([0.15, 0.20, 0.30, 0.50, 0.70, 1.00])
#ax.set_xticklabels(['0.15', '0.20', '0.30', '0.50', '0.70', '1.00'])
#ax.xaxis.set_tick_params(labelsize=18)
#ax.set_yticks([0.010, 0.015, 0.020, 0.030])
#ax.set_yticklabels(['0.010', '0.015', '0.020', '0.030'])
#ax.yaxis.set_tick_params(labelsize=18)

plt.savefig(filename[:-4] + '.png', bbox_inches='tight')
plt.show()

