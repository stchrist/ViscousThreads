import numpy as np
import matplotlib.pyplot as plt
import sys

#data = np.genfromtxt('rewards.dat', invalid_raise=False, filling_values=0)

filename = sys.argv[1]

#read into 2d list (assuming empty lines mean a new row has to be added)
data = []
data.append([])

foundOneNewline = True
rowIdx = 0

with open(filename) as f:
	lines = f.readlines()
	for line in lines:
	    # if line == '\n':
            if not line or line == '\n' or line.startswith('#') or line.startswith(' #'):
                if foundOneNewline == False:
		    data.append([])
                    foundOneNewline = True
                    rowIdx = rowIdx + 1
	    else:
                data[rowIdx].append(float(line))
                foundOneNewline = False

if len(data[rowIdx]) == 0:
    data = data[:-1]

#fill missing values
#maxlen = 0
#for i in range(0,len(data)):
#	if len(data[i]) > maxlen:
#		maxlen = len(data[i])
#
#for i in range(0,len(data)):
#	if len(data[i]) < maxlen:
#		missing = maxlen - len(data[i])
#		data[i] += missing * [0]

#make all to array
for i in range(0,len(data)):
	data[i] = np.array(data[i])
dataarray = np.array(data)


#nvals = 10000
#avg = np.mean(dataarray[:,:nvals], axis=0)
avg = np.mean(dataarray[:,:], axis=0)
#print avg


def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth



def set_style():
	#plt.style.use(['seaborn-white', 'seaborn-paper'])
	plt.style.use(['seaborn-paper'])
	plt.rcParams["font.family"] = "Times", "Times New Roman"




set_style()


fig, ax = plt.subplots()
ax.plot(avg, label="Average reward over all runs")
#ax.plot(smooth(avg, 20), 'r', label="Smoothed over 20 episodes")


# Set axis limits
#ax.set_xlim(0, 1400)
#ax.set_ylim(-0.4,0.4)
#ax.set_ylim(ymin=0)

#plt.ylim(ymin=-100000)
#ax.legend(loc=4)
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
ax.set_xlabel("Episode", fontsize=20)
#ax.set_ylabel("Average reward", rotation=0, fontsize=20)
ax.set_ylabel("Reward", rotation=0, fontsize=20)
ax.yaxis.set_label_coords(0.08,1.05)


# Set axis ticks
#ax.minorticks_off()
#ax.set_xticks([0.15, 0.20, 0.30, 0.50, 0.70, 1.00])
#ax.set_xticklabels(['0.15', '0.20', '0.30', '0.50', '0.70', '1.00'])
ax.xaxis.set_tick_params(labelsize=16)
#ax.set_yticks([0.010, 0.015, 0.020, 0.030])
#ax.set_yticklabels(['0.010', '0.015', '0.020', '0.030'])
ax.yaxis.set_tick_params(labelsize=16)



#ax.title("Rewards, %i runs" % len(data))

plt.tight_layout()

plt.savefig("rewards.pdf", bbox_inches='tight')
plt.show()
