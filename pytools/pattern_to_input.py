# Convert created output pattern (each row has coordinates of one point) to pattern format used for learning (one long row with alternating x y coordinates for all points)
#
# Usage: pattern_to_input.py filename [starting index]

import sys
import numpy as np


filename = sys.argv[1]
data = np.loadtxt(filename)


startidx = 0
if len(sys.argv) > 2:
	startidx = int(sys.argv[2])

f = open(filename[:-4] + ".coord", 'w')

for i in range(startidx, len(data)):
	f.write(str(data[i,0]))
	f.write(",")
	f.write(str(data[i,1]))
	if i < len(data)-1:
		f.write(",")

f.close()
