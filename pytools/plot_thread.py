import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

filename = sys.argv[1]
data = np.loadtxt(filename)

x=data[:,0]
y=data[:,1]
z=data[:,2]

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.scatter(x, y, z) #, s=5)
ax.plot3D(x,y,z)
#N = len(z)
#normalize_factor = max(z) - min(z)
#for i in xrange(N-1):
#    ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], color=plt.cm.jet(255*z[i]/normalize_factor))


# Create cubic bounding box to simulate equal aspect ratio
max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max()

mid_x = (x.max()+x.min()) * 0.5
mid_y = (y.max()+y.min()) * 0.5
mid_z = (z.max()+z.min()) * 0.5
ax.set_xlim(mid_x - max_range, mid_x + max_range)
ax.set_ylim(mid_y - max_range, mid_y + max_range)
ax.set_zlim(mid_z - max_range, mid_z + max_range)



ax.set_zlim(0, 1.25)
ax.set_aspect('equal')
ax.view_init(elev=10., azim=45)

plt.show()
#plt.plot(x, y, z);
