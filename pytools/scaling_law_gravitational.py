import sys
import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]
data = np.loadtxt(filename)

dt = data[:,0]
Q = data[:,1]
H = data[:,2]
nu = data[:,3]
g = data[:,4]
r = data[:,5]
freq = data[:,6]


fig, (ax2) = plt.subplots(1,1)
#fig.tight_layout()
#fig.subplots_adjust(left=0.1, top=0.9, wspace=0.5)

xs = r
xs = np.append(xs, max(r)*2)
xs = np.append(xs, min(r)*0.5)

#Fit line to curve in first plot
b = freq / pow(Q, 0.75) / pow(g, 0.25) * pow(nu, 0.25)
coefficients = np.polyfit(np.log10(r), np.log10(b), 1)

ys = pow(10, coefficients[1]) * pow(xs, coefficients[0])
ax2.loglog(xs, ys, 'r')
print b
print ys
print coefficients[0], coefficients[1]


ax2.loglog(r, freq / pow(Q, 0.75) / pow(g, 0.25) * pow(nu, 0.25), 's')
#ax2.loglog(r, pow(r, -2) * 0.48765573032)
ax2.loglog(xs, pow(xs, -2) * 0.49)
ax2.set_title("Gravitational coiling \n(slope should be -2)")
ax2.set_xlabel(r"$r$")
ax2.set_ylabel(r"$\Omega Q^{-0.75} g^{-0.25} \nu^{0.25}$")

plt.savefig(filename[:-4] + '.png')
plt.show()
