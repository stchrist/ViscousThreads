import sys
import numpy as np

filename = sys.argv[1]
data = np.loadtxt(filename)

firstidx = '02000'
if len(sys.argv) > 2:
    firstidx = sys.argv[2]

data1000 = np.loadtxt(filename[:-1*(len(firstidx)+4)] + firstidx + filename[-4:])

x = data[len(data1000):,0]
t = data[len(data1000):,3]

sign_change = []

for i in range(1, len(x)):
    if x[i] > 0 and x[i-1] <= 0:
        sign_change.append(t[i])
    elif x[i] < 0 and x[i-1] >= 0:
        sign_change.append(t[i])

difference = np.diff(sign_change)

freq = 1.0 / (2.0 * np.mean(difference))

print "Coiling frequence = ", freq

r = np.mean(data[len(data1000):,2])

print "Radius = ", r
