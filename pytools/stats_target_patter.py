import numpy as np
from sys import argv

filename = argv[1]
data = np.loadtxt(filename, delimiter=',')

x = data[::2]
y = data[1::2]

print("x-range: [%f, %f], length = %f" % (np.min(x), np.max(x), np.max(x)-np.min(x)))
print("y-range: [%f, %f], length = %f" % (np.min(y), np.max(y), np.max(y)-np.min(y)))
