import numpy as np
import sys

data = np.loadtxt(sys.argv[1], delimiter=',')

x = data[::2]
y = data[1::2]

datanew = np.zeros(len(data))
datanew[::2] = x[::1]
datanew[1::2] = -y[::1] + np.max(y)

np.savetxt(sys.argv[1] + "_new", [datanew], delimiter=',', fmt='%f')

