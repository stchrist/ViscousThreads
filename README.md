# Learning to write with viscous threads

The goal of this project is to learn how to write with viscous threads using reinforcement learning.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See running for notes on how to run the project on Odyssey (or other clusters).

### Prerequisites

The code was only tested on Linux systems and uses the following tools and libraries:

* g++ 6.1 or greater
* CMake 3.1 or greater
* Eigen3
* LAPACK
* MPICH
* OpenBLAS

### Obtaining the code
The code is located in this gitlab repository and is using git-submodules to include the Smarties library. All commands starting with git submodule have to be run from the toplevel of the working tree.
The instructions for working with submodules are from this [Medium Article]. The article includes more instructions, e.g. how to add a new submodule to a repository.

We use git-lfs for some file types (.png, .pdf, .raw).

#### Cloning the repository including submodules
```sh
git clone <repository>
git submodule update --init
```
or
```sh
git clone --recursive <repository>
```

#### Pulling new commits for an existing submodule
```sh
cd <submodule directory>
git fetch
git log --oneline origin/master -10
git checkout -q <SHA1>
```

```<SHA1>``` on the last line refers to the commit that you want to point the submodule to. It would most likely be the newest commit that is shown by git log on the previous line.

**Don't forget to push after updating the submodule! We want all copies of the repo to point ot the same submodule commit!**

```sh
git add <submodule directory>
git commit -m "<message>"
git push
```

#### Pulling a repo that uses submodules
To pull changes from a repository that uses submodules, just doing ```git pull``` is not enough, as it doesn't update submodules.
```sh
git pull
git submodule update --init --recursive
```
**Git auto-fetches, but does not auto-update.**

### Structure

* src: All source code
	* viscousThreads: Code that runs the simulation of a viscous thread
	* simulation: Main file to run a single simulation
	* third_party: Third party code that is used in this project
		* filesystem: Replacement for the Boost filesystem library
		* smarties: Reinforcement Library by Guido Novati. See the README files within Smarties for a documentation of the library.
		* smarties_odyssey: This directory has the same structure as smarties. It contains files from smarties that we needed to modify to run it on Odyssey, or additional files for Smarties. Those files will be copied over before we run Smarties on Odyssey, the instructions are below. **Keep in mind that those files have to be updated should the corresponding file change in Smarties! I recommend copying the files over to smarties_odyssey when they change within Smarties.**
	* **smartiesApp**: Code that sets up the learning problem to be run with Smarties. This is where most of the work will happen.
* patterns: Template for the patterns that we want to learn to write, the file structure is alternating x and y coordinates for all points ```[x y x y x y ...]```
* results: Results from different runs
* pytools: Python scripts

As you can see, the structure within src is pretty complicated right now. It could probably be simplified by using smarties as the base folder, and creating a directory for our application within smarties/apps. All the necessary source files could go there.
Note that there will already appear to be an app folder for this project, once the code has been run. The reason is that the smartiesApp directory will be copied there. It is originally located somewhere else for structural reasons within the git repository.

### Compiling

The project will be compiled using CMake.

Go to the project directory and create a build directory:
```sh
cd your/project/path
mkdir build
```

Enter build directory:
```sh
cd build
```

Run CMake:
```sh
cmake ..
```

Run make:
```sh
make
```

The executables will be located in the build directory, following the same folder structure as the src directory.

The Smarties library will also be built using the commands above, but it won't be cleaned before building and thus occasionally gives errors. To rebuild Smarties, use the following:
```sh
cd src/third_party/smarties/makefiles
make clean
make
```

## Running

The code can be run on a local machine and on Odyssey. However, only single simulations can be run locally, the learning algorithm has to be run on Odyssey. Some basic instructions are here, but more details can be found in the Smarties README files in each directory.

### Local

Run
```sh
./build/src/simulation/viscousThreadsSimulation
```
to run a single simulation. The parameters all have to be speciefied within the source code. So far, command line parameters have not been implemented. This means that the code needs to be recompiled for parameter changes.


### Odyssey

Odyssey should be used to run the reinforcement learning problem. The Smarties library's run files have been adapted to work on Odyssey.

The first time you run the code on Odyssey, the adapted files have to be copied over to their correct location:
```sh
cp src/third_party/smarties_odyssey/* src/third_party/smarties/ -r
```

To run the code, fiirst, load the required modules (in the base of the project directory, not within smarties):
```sh
source load_modules
```
The result is that there are some modified files within Smarties. Before pulling from Smarties, they have to be stashed and afterwards unstashed.

**Note: The modules have to be loaded every time you reconnect to Odyssey, while the files only have to be copied over once after cloning the repository.**

Then go to the smarties launch folder:
```sh
cd src/third_party/smarties/launch
```

Run the launch command. It will submit a batch job and run it in the specified run directory. The run directory name can be anything, as it will be created first. From my experience, it is preferable to not reuse names until the previous run directory is deleted. Run directories are created within runs/. This is also where the output will be.
```sh
./launch.sh <run_dir> viscous_threads <settings_file, probably settings/settings_RACER.sh>
```

The results of the run are in
```
<base_dir>/<run_dir>
```

Use the scripts in ```smarties/pytools/``` to display the results, or look at the plot the created patterns by using the scripts in ```pytools``` (note: not within Smarties, in the base folder).
```
<base_dir>/<run_dir>/output_patterns/
```

The location of ```<base_dir>``` is different across systems. On Odyssey, it will be in ```/n/regal/mahadevan_lab/<username>```, on your local machine it will be ```runs```. This behavior can be changed within Smarties.

## Authors

* **Stephanie Christ** - *Initial work* - [Email](mailto:stephanie-m@gmx.ch)
* **Guido Novati** - *Smarties RL Library* - [Repository](https://github.com/cselab/smarties), [Paper](https://arxiv.org/abs/1807.05827) - G. Novati and P. Koumoutsakos, "Remember and Forget for Experience Replay," arXiv preprint arXiv:1807.05827, 2018


[Medium Article]: <https://medium.com/@porteneuve/mastering-git-submodules-34c65e940407>
