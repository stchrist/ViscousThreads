cp ../apps/viscous_threads/launch.sh ${BASEPATH}${RUNFOLDER}/launchSim.sh
cp ../apps/viscous_threads/viscousThreadsApp ${BASEPATH}${RUNFOLDER}/
cp ../apps/viscous_threads/viscous-threads-app.cpp ${BASEPATH}${RUNFOLDER}/

rm -f -r ${BASEPATH}${RUNFOLDER}/patterns/
mkdir ${BASEPATH}${RUNFOLDER}/patterns/
cp ../../../../patterns/*.coord ${BASEPATH}${RUNFOLDER}/patterns/

mkdir -p ${BASEPATH}${RUNFOLDER}/output_patterns/
