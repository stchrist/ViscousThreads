# Launch the environment
#
# This file needs to contain one uncommented line in the following format:
#
# ../viscousThreadsApp $1 ../patterns/PATTERN_FILE PATTERN_SCALE_FACTOR METHOD_NAME (PATTERN OUTPUT INTERVAL) (STEPS PER ADVANCE) (DT) (DX) (H0)
#
# METHOD NAME:
#        Name of the method by which states/actions are defined
# PATTERN SCALE FACTOR:
#        Every point in the target pattern gets multiplied by this scaling factor.
#        This helps get the pattern to a scale which matches the simulation scale.
# PATTERN OUTPUT INTERVAL: (optional)
#        Define how often you want to write the output pattern to a file. 0 if never.
#        Default value: 0
# STEPS PER ADVANCE: (optional)
#        Number of simulations steps to run in learning "advance"
#        Default value: 20
# DT: (optional)
#        Simulation time step
#        Default value: 0.02
# DX: (optional)
#        Simulation space discretization
#        Default value: 0.02
# H0: (optional)
#        Initial nozzle height
#        Default value: 0.5
#
# Use "" to use the default value of an optional argument while defining one that comes after.
# E.g. ../viscousThreadsApp $1 ../patterns/simple-wave.coord 0.035 xyz_phi_theta 1000 "" 0.02
#      DT = 0.02 (default value), but DX = 0.02 (not default value)

#../viscousThreadsApp $1 ../patterns/line.coord 5.0 xy_theta 1000
../viscousThreadsApp $1 ../patterns/simple-wave.coord 0.035 xyz_phi_theta 1000
#../viscousThreadsApp $1 ../patterns/St.coord 0.05 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/Stephanie.coord 0.022 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/Mahadevan.coord 0.03 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/Brentwood.coord 0.045 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/cat.coord 0.063 xy_theta 1000
