//
// Created by Stephanie Christ on 18.10.2017.
//

#include <iostream>

#include "Simulation.h"
#include <Eigen/Dense>
#include <Eigen/QR>
//#include <Eigen/SparseCore>
//#include <Eigen/SparseCholesky>
#include <ctime>
#ifdef ODYSSEY
    #include <filesystem/filesystem/path.h>
    #include <fstream>
#else
    #include <boost/filesystem/fstream.hpp>
#endif

#include "projectRootDir.h"
#include "BandedMatrix.h"




//int PEND_DIR = 1;


//This below was used so far for learning alg
//Simulation::Simulation() : Simulation(0.025, 1.25, (Vector3() << 0.8, 0.0, 0.0).finished()) {}
Simulation::Simulation() : Simulation(0.02, 0.025, 0.5, (Vector3() << 0.8, 0.0, 0.0).finished()) {}

Simulation::Simulation(const float_type dt, const float_type lc, const float_type h, const Vector3 container_velocity)
        : n_steps_(3000), dt_(dt), total_time_(n_steps_ * dt_), time_step_(0), lc_(lc),
          collision_mode_(cc), subdivision_criterion_(scalingLaw),
          rho_(5e-4), mu_(0.2), gamma_(0.0), Qc_(3.96e-3), Ac_(6.44e-3), h_(h), gravity_(9.81),
          floor_shift_(Vector2().setZero()), floor_velocity_((Vector2() << 0.0, 0.0).finished()),
          container_position_((Vector3() << 0.0, 0.0, h_).finished()),
          container_velocity_(container_velocity),
          phi_(0.0), theta_(M_PI),
          u_clamp_0(Qc_ / Ac_ * (Vector3() << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_)).finished() + container_velocity_),
          u_clamp_1(u_clamp_0),
          v_clamp_1(0.0),
          u_clamp_2((Vector3() << floor_velocity_(0), floor_velocity_(1), 0.0).finished()),
          v_clamp_2(0.0),
          file_output_(true), output_freq_(100), rootPath_(getProjectRootDir())
{
    Eigen::initParallel();

    //speed with which fluid comes out of container
    float_type Uc = Qc_ / Ac_;

    //initialize vertices so that the last vertex is touching the floor
    n_segments_ = std::ceil(h_ / lc_) + 2;

    float l = h_ / std::cos(5.0 * M_PI / 180.0); //we want initial condition to be at a 20 deg angle
    n_segments_ = std::ceil(l / lc_) + 2;
    float w = std::sqrt(l*l - h_*h_);
    float hc = h_ * lc_ / l;
    float wc = w * lc_ / l;

    n_floor_vertices_ = 2;

    X.resize(3*(n_segments_+1));
    X.setZero();

    U.resize(4*n_segments_ + 3);
    U.setZero();

//    X.head(6) << 0, 0, h_ + lc_,
//                 0, 0, h_;

    Vector3 dir; //direction of nozzle
    dir << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_);

    X.head(3) = container_position_ - lc_ * dir;
    X.segment(3, 3) << container_position_;

    //flow direction is not z direction anymore!
    U.head(3) = u_clamp_0;
    U.segment(4, 3) = u_clamp_1;

    volume_.resize(n_segments_, lc_ * Ac_);
    mass_.resize(n_segments_, rho_ * lc_ * Ac_);

    float_type z = 0;

    //last segment lays on floor -> last vertex is on floor, and has distance lc_ to second to last vertex
    X.segment(3*n_segments_, 3) << lc_, w, z;
    U.segment(4*n_segments_, 3) << 0, 0, -Uc;

    for (int i=n_segments_-1; i>=2; --i) {
        X.segment(3*i, 3) << 0, w, z;
        z += hc;
        w -= wc;

        U.segment(4*i, 3) << 0, 0, -Uc; //TODO: change this velocity?
    }

    volume_[1] = Ac_ * (X.segment(3, 3) - X.segment(6, 3)).norm();
    mass_[1] = rho_ * volume_[1];


    std::time_t time = std::time(nullptr);
    std::strftime(&date_time_string_[0], 30, "%F_%T", std::localtime(&time));

    writeThread();
}

void Simulation::run() {
    for (int i=0; i<n_steps_; ++i) {
//        step(dt_);
        step_lapack(dt_);

        if (file_output_ && !(time_step_ % output_freq_ )) {
            writeThread();
            writePattern();
        }

        if (!(time_step_% 10)) {
            std::cout << "Time step " << time_step_ << " done!\n\n";
        }
    }
}

inline MatrixX calculateV3(Vector3 t, float_type l) {
    MatrixX V3(3, 7);
    V3.setZero();

    V3(0, 0) = -1.0 + t(0) * t(0);
    V3(0, 1) = t(0) * t(1);
    V3(0, 2) = t(0) * t(2);
    V3(0, 4) = 1.0 - t(0) * t(0);
    V3(0, 5) = -t(0) * t(1);
    V3(0, 6) = -t(0) * t(2);

    V3(1, 0) = t(1) * t(0);
    V3(1, 1) = -1.0 + t(1) * t(1);
    V3(1, 2) = t(1) * t(2);
    V3(1, 4) = -t(1) * t(0);
    V3(1, 5) = 1.0 - t(1) * t(1);
    V3(1, 6) = -t(1) * t(2);

    V3(2, 0) = t(2) * t(0);
    V3(2, 1) = t(2) * t(1);
    V3(2, 2) = -1.0 + t(2) * t(2);
    V3(2, 4) = -t(2) * t(0);
    V3(2, 5) = -t(2) * t(1);
    V3(2, 6) = 1.0 - t(2) * t(2);

    V3 /= l;

    return V3;
}


inline MatrixX calculateW3(Vector3 t, float_type l) {
    MatrixX W3(3, 7);
    W3.setZero();

    W3(0, 1) = t(2);
    W3(0, 2) = -t(1);
    W3(0, 5) = -t(2);
    W3(0, 6) = t(1);

    W3(1, 0) = -t(2);
    W3(1, 2) = t(0);
    W3(1, 4) = t(2);
    W3(1, 6) = -t(0);

    W3(2, 0) = t(1);
    W3(2, 1) = -t(0);
    W3(2, 4) = -t(1);
    W3(2, 5) = t(0);

    W3 /= l;

    W3.block(0, 3, 3, 1) << t;

    return W3;
}


void Simulation::step(const float_type dt) {
    int n = n_segments_ - 1; //highest index of segments [0,n]

    //store current state of simulation
    prevX = X;
    prevU = U;
    prevMass_ = mass_;
    prevVolume_ = volume_;

    //calculate boundary matrix (clamp-clamp boundary conditions)
    int r;

    switch (n_floor_vertices_) {
        case 0: r = 4*n; break;
        case 1: r = 4*n - 3; break;
        default: r = 4*n - 7; break;
    }


    if (r > 0) {
        MatrixX B_mat(4*n+7, r);
        B_mat.setZero();

        B_mat.block(7, 0, r, r).setIdentity();

        VectorX B_vec(4 * n + 7);
        B_vec.setZero();

        //TODO: should I include container velocity in u_clamp_1? Probably, since u_clamp_2 includes floor velocity
        //TODO: what to do if I'm changing angles?
        B_vec.head(3) << u_clamp_0; //TODO: do I need container velocity here? What if I move container, but don't have fixed velocity?
        B_vec(3) = v_clamp_1;
        B_vec.segment(4, 3) << u_clamp_1;


        if (n_floor_vertices_ > 0) {
            B_vec.segment(4 * n + 4, 3) << u_clamp_2;

            if (n_floor_vertices_ > 1) {
                B_vec.segment(4 * n, 3) << u_clamp_2;
                B_vec(4 * n + 3) = v_clamp_2;
            }
        }


        //do dynamic sequence

        //calculate Rayleigh potential D, generalized mass matrix M, external force vector F and mapping matrix UX


        int nthreads = omp_get_max_threads();
//        int nthreads = 1;
        std::vector<MatrixX> D_vec(nthreads, MatrixX(4 * n + 7, 4 * n + 7).setZero());
        std::vector<MatrixX> M_vec(nthreads, MatrixX(4 * n + 7, 4 * n + 7).setZero());
        std::vector<VectorX> F_vec(nthreads, VectorX(4 * n + 7).setZero());
        std::vector<MatrixX> UX_vec(nthreads, MatrixX(3 * (n + 2), 4 * n + 7).setZero());

        Vector3 t_last;
        float_type ny_last = 0;

        //loop over segments
#pragma omp parallel for default(shared) schedule(static)
        for (int i = 0; i <= n; ++i) {
            int threadid = omp_get_thread_num();
//            int threadid = 0;

            MatrixX& D = D_vec[threadid];
            MatrixX& M = M_vec[threadid];
            VectorX& F = F_vec[threadid];
            MatrixX& UX = UX_vec[threadid];



            Vector3 t = X.segment(3 * (i + 1), 3) - X.segment(3 * i, 3);
            float_type l = t.norm();
            t.normalize();


            //need to construct them so that the product with the vector of virtual velocities gives the right result

            //Ls (stretch): defined for all segments
            VectorX Ls(4 * n + 7);
            Ls.setZero();
            Ls.segment(4 * i, 3) << -t;
            Ls.segment(4 * (i + 1), 3) << t;

            float_type A = volume_[i] / l;
            float_type Di = 3.0 * mu_ * A / l;

            //add stretch contribution to D
            D.noalias() += Di * Ls * Ls.transpose();


            //time derivative of the unit tangent (defined for segments), see eq. (43)
            MatrixX V3(3, 7);
            V3 = calculateV3(t, l);

            MatrixX V(4 * n + 7, 4 * n + 7);
            V.setZero();
            V.block(4*i, 4*i, 3, 7) << V3;

            MatrixX W3(3, 7);
            W3 = calculateW3(t, l);

            MatrixX W(4 * n + 7, 4 * n + 7);
            W.setZero();
            W.block(4*i, 4*i, 3, 7) << W3;

            Vector3 t_before;
            float_type ny_before;

            if (i > 0) {
                t_before = X.segment(3 * i, 3) - X.segment(3 * (i - 1), 3);
                float_type l_before = t_before.norm();
                t_before.normalize();

                float_type A_before = volume_[i-1] / l_before;
                float_type a_before = std::sqrt(A_before / M_PI);
                ny_before = M_PI * gamma_ * a_before;

                MatrixX V3_before = calculateV3(t_before, l_before);
                MatrixX W3_before = calculateW3(t_before, l_before);


                //discrete binormal curvature vector (defined on inner vertices)
                float tdt = t_before.dot(t);
                Vector3 K3 = t_before.cross(t) / (0.5 * (1.0 + tdt));


                //Lt (twist): defined for all inner vertices, see eq. (50b)
                VectorX Lt(4 * n + 7);
                Lt.setZero();
                Lt.segment(4*(i-1), 7).noalias() += 0.5 * V3_before.transpose() * K3;
                Lt.segment(4*i, 7).noalias() += 0.5 * V3.transpose() * K3;
                Lt(4 * (i - 1) + 3) += -1;
                Lt(4 * i + 3) += 1;


                //vertex based tangent
                Vector3 t_tilde = t_before + t;
                t_tilde.normalize();


                //Lb (bend): defined for all inner vertices, see eq. (51b)

                MatrixX Lb(4 * n + 7, 4 * n + 7);
                Lb.setZero();
                Lb.block(4*i, 4*(i-1), 3, 7).noalias() += -W3_before;
                Lb.block(4*i, 4*i, 3, 7).noalias() += W3;
                Lb.block(4*i, 4*(i-1), 3, 7).noalias() += t_tilde * (t_tilde.transpose() * W3_before);
                Lb.block(4*i, 4*i, 3, 7).noalias() += -t_tilde * (t_tilde.transpose() * W3);


                float_type mu_l = 0.5 * (mu_ * A_before * A_before + mu_ * A * A) / (4.0 * M_PI);
                float_type Ci = 2.0 * mu_l / (0.5 * (l_before + l));
                float_type Bi = 3.0 * mu_l / (0.5 * (l_before + l));

                //add twist contribution to D
                D.noalias() += Ci * Lt * Lt.transpose();
                //add bend contribution to D
                D.noalias() += Bi * Lb.transpose() * Lb;
            }



            //update mass matrix and force
            float_type a = std::sqrt(A / M_PI);
            float_type ny = M_PI * gamma_ * a;

            float_type mass;
            Vector3 f;
            if (i == 0) {
                mass = 0.5 * mass_[i];
                f = ny * t;
            } else {
                mass = 0.5 * (mass_[i - 1] + mass_[i]);
                f = ny * t - ny_before * t_before;
            }
            M.block(4*i, 4*i, 3, 3).diagonal() << mass, mass, mass;

            F.segment(4*i, 3) << f; //force from surface tension
            F(4 * i + 2) += -mass * gravity_; //gravity

            //update UX transformation matrix
            UX.block(3 * i, 4 * i, 3, 3).setIdentity();


            if (i == n) {
                t_last = t;
                ny_last = ny;
            }
        }




        MatrixX D = D_vec[0];
        MatrixX M = M_vec[0];
        VectorX F = F_vec[0];
        MatrixX UX = UX_vec[0];

        for (int i=1; i<nthreads; ++i) {
            D += D_vec[i];
            M += M_vec[i];
            F += F_vec[i];
            UX += UX_vec[i];
        }




        //last vertex

        float_type mass = 0.5 * mass_[n];
        M.block(4*(n+1), 4*(n+1), 3, 3).diagonal() << mass, mass, mass;

        F.segment(4*(n+1), 3) << -ny_last * t_last;
        F(4 * (n + 1) + 2) += -mass * 9.81;

        UX.block(3 * (n + 1), 4 * (n + 1), 3, 3).setIdentity();



        //solve for Y
        MatrixX Bt = B_mat.transpose();
        MatrixX A = Bt * (M + dt * D) * B_mat;
        VectorX b = Bt * (dt * (-D * B_vec + F) - M * (B_vec - U));
        VectorX Y = A.partialPivLu().solve(b); //maybe use partialPivLu, because it is multithreaded

        //reconstruct U after time step
        U = B_mat * Y + B_vec;

        //update X
        X = X + dt * UX * U;

        //end of dynamical sequence
    }


    //enforce inflow boundary condition (container)
    enforceBC();

    //treat collisions with the floor (depends on c)
    bool done = collisions();

    if (!done) {
        //don't continue if collisions are not treated yet (rollback mode)
        return;
    }

    //optionally, refine by subdivision (based on criterion f)
    meshRefinement();

    //update floor and container position (motion is prescribed)
    integrateContainerPos(container_velocity_, phi_velocity_, theta_velocity_);

    //Update container velocities (if required)
    //This needs to be commented out for the learning algorithm.
    //I'm leaving an example here for reference.

//    if (time_step_ > 18380) {
//        floor_velocity_(1) += 0.02 * (n_steps_ - 18380);
//    }

    ++time_step_;
}

extern "C" void sgbmv_(const char *TRANS, const long *M, const long *N, const long *KL, const long *KU,
                       const float *alpha, float *A, const long *LDA, float *X, const long *LDX,
                       const float *beta, float *Y, const long *INCY);

extern "C" void saxpy_(const long *N, const float *SA, const float *SX, const long *INCX, float *SY, const long *INCY);

extern "C" void sgbsv_(const long *N, const long *KL, const long *KU, const long *NRHS, float *AB,
                       const long *LDAB, long *IPIV, float *B, const long *LDB, long *INFO);

extern "C" void scopy_(const long *N, const float *X, const long *INCX, float *Y, const long *INCY);


extern "C" void dgbmv_(const char *TRANS, const long *M, const long *N, const long *KL, const long *KU,
                       const double *alpha, double *A, const long *LDA, double *X, const long *LDX,
                       const double *beta, double *Y, const long *INCY);

extern "C" void daxpy_(const long *N, const double *SA, const double *SX, const long *INCX, double *SY, const long *INCY);

extern "C" void dgbsv_(const long *N, const long *KL, const long *KU, const long *NRHS, double *AB,
                       const long *LDAB, long *IPIV, double *B, const long *LDB, long *INFO);

extern "C" void dgbsvx_(const char *FACT, const char *TRANS, const long *N, const long *KL, const long *KU,
                        const long *NRHS, double *AB, const long *LDAB, double *AFB, const long *LDAFB, long *IPIV,
                        char *EQUED, double *R, double *C, double *B, const long *LDB, double *X, const long *LDX,
                        double *RCOND, double *FERR, double *BERR, double *WORK, long *IWORK, long *INFO);

extern "C" void dcopy_(const long *N, const double *X, const long *INCX, double *Y, const long *INCY);

extern "C" void spbsvx_(const char *FACT, const char *UPLO, const long *N, const long *KD,
                        const long *NRHS, float *AB, const long *LDAB, float *AFB, const long *LDAFB,
                        char *EQUED, float *S, float *B, const long *LDB, float *X, const long *LDX,
                        float *RCOND, float *FERR, float *BERR, float *WORK, long *IWORK, long *INFO);



void Simulation::step_lapack(const float_type dt) {
    int n = n_segments_ - 1; //highest index of segments [0,n]

    //calculate boundary matrix (clamp-clamp boundary conditions)
    int r;

    switch (n_floor_vertices_) {
        case 0: r = 4*n; break;
        case 1: r = 4*n - 3; break;
        default: r = 4*n - 7; break;
    }


    if (r > 0) {
        BandedMatrix B_mat(4*n + 7, 4*n + 7, 0, 0);

        std::vector<float_type> B_vec(4 * n + 7, 0);
        B_vec[0] = u_clamp_0[0];
        B_vec[1] = u_clamp_0[1];
        B_vec[2] = u_clamp_0[2];
        B_vec[3] = v_clamp_1;
        B_vec[4] = u_clamp_1[0];
        B_vec[5] = u_clamp_1[1];
        B_vec[6] = u_clamp_1[2];


        if (n_floor_vertices_ > 0) {
            B_vec[4 * n + 4] = u_clamp_2[0];
            B_vec[4 * n + 5] = u_clamp_2[1];
            B_vec[4 * n + 6] = u_clamp_2[2];

            if (n_floor_vertices_ > 1) {
                B_vec[4 * n] = u_clamp_2[0];
                B_vec[4 * n + 1] = u_clamp_2[1];
                B_vec[4 * n + 2] = u_clamp_2[2];
                B_vec[4 * n + 3] = v_clamp_2;
            }
        }


        //do dynamic sequence

        //calculate Rayleigh potential D, generalized mass matrix M, external force vector F and mapping matrix UX

        BandedMatrix D(4 * n + 7, 4 * n + 7, 10, 20);
        BandedMatrix M(4 * n + 7, 4 * n + 7, 0, 0);
        std::vector<float_type> F(4 * n + 7, 0);
        BandedMatrix UX(3 * (n + 2), 4 * n + 7, 0, n+2);

        Vector3 t_last;
        float_type ny_last = 0;

        //loop over segments
//#pragma omp parallel for default(shared)
        for (int i = 0; i <= n; ++i) {
//            int threadid = omp_get_thread_num();


            if (i > 1 && i <= n+1 - n_floor_vertices_) {
                //free vertex
                B_mat(4*i, 4*i) = 1;
                B_mat(4*i + 1, 4*i + 1) = 1;
                B_mat(4*i + 2, 4*i + 2) = 1;
                B_mat(4*i + 3, 4*i + 3) = 1;
            }



            Vector3 t = X.segment(3 * (i + 1), 3) - X.segment(3 * i, 3);
            float_type l = t.norm();
            t.normalize();


            //need to construct them so that the product with the vector of virtual velocities gives the right result

            //Ls (stretch): defined for all segments
            float_type A = volume_[i] / l;
            float_type Di = 3.0 * mu_ * A / l;
            Di *= dt;
//            Di = 0;

            //add stretch contribution to D
            D(4*i + 0, 4*i + 0) += Di * t(0) * t(0);
            D(4*i + 0, 4*i + 1) += Di * t(0) * t(1);
            D(4*i + 0, 4*i + 2) += Di * t(0) * t(2);
            D(4*i + 0, 4*i + 4) += -Di * t(0) * t(0);
            D(4*i + 0, 4*i + 5) += -Di * t(0) * t(1);
            D(4*i + 0, 4*i + 6) += -Di * t(0) * t(2);

            D(4*i + 1, 4*i + 0) += Di * t(1) * t(0);
            D(4*i + 1, 4*i + 1) += Di * t(1) * t(1);
            D(4*i + 1, 4*i + 2) += Di * t(1) * t(2);
            D(4*i + 1, 4*i + 4) += -Di * t(1) * t(0);
            D(4*i + 1, 4*i + 5) += -Di * t(1) * t(1);
            D(4*i + 1, 4*i + 6) += -Di * t(1) * t(2);

            D(4*i + 2, 4*i + 0) += Di * t(2) * t(0);
            D(4*i + 2, 4*i + 1) += Di * t(2) * t(1);
            D(4*i + 2, 4*i + 2) += Di * t(2) * t(2);
            D(4*i + 2, 4*i + 4) += -Di * t(2) * t(0);
            D(4*i + 2, 4*i + 5) += -Di * t(2) * t(1);
            D(4*i + 2, 4*i + 6) += -Di * t(2) * t(2);

            D(4*i + 4, 4*i + 0) += -Di * t(0) * t(0);
            D(4*i + 4, 4*i + 1) += -Di * t(0) * t(1);
            D(4*i + 4, 4*i + 2) += -Di * t(0) * t(2);
            D(4*i + 4, 4*i + 4) += Di * t(0) * t(0);
            D(4*i + 4, 4*i + 5) += Di * t(0) * t(1);
            D(4*i + 4, 4*i + 6) += Di * t(0) * t(2);

            D(4*i + 5, 4*i + 0) += -Di * t(1) * t(0);
            D(4*i + 5, 4*i + 1) += -Di * t(1) * t(1);
            D(4*i + 5, 4*i + 2) += -Di * t(1) * t(2);
            D(4*i + 5, 4*i + 4) += Di * t(1) * t(0);
            D(4*i + 5, 4*i + 5) += Di * t(1) * t(1);
            D(4*i + 5, 4*i + 6) += Di * t(1) * t(2);

            D(4*i + 6, 4*i + 0) += -Di * t(2) * t(0);
            D(4*i + 6, 4*i + 1) += -Di * t(2) * t(1);
            D(4*i + 6, 4*i + 2) += -Di * t(2) * t(2);
            D(4*i + 6, 4*i + 4) += Di * t(2) * t(0);
            D(4*i + 6, 4*i + 5) += Di * t(2) * t(1);
            D(4*i + 6, 4*i + 6) += Di * t(2) * t(2);




            //time derivative of the unit tangent (defined for segments), see eq. (43)
            MatrixX V3 = calculateV3(t, l);

            MatrixX W3 = calculateW3(t, l);

            Vector3 t_before;
            float_type ny_before;

            if (i > 0) {
                t_before = X.segment(3 * i, 3) - X.segment(3 * (i - 1), 3);
                float_type l_before = t_before.norm();
                t_before.normalize();

                float_type A_before = volume_[i-1] / l_before;
                float_type a_before = std::sqrt(A_before / M_PI);
                ny_before = M_PI * gamma_ * a_before;

                MatrixX V3_before = calculateV3(t_before, l_before);
                MatrixX W3_before = calculateW3(t_before, l_before);


                //discrete binormal curvature vector (defined on inner vertices)
                float tdt = t_before.dot(t);
                Vector3 K3 = t_before.cross(t) / (0.5 * (1.0 + tdt));


                //Lt (twist): defined for all inner vertices, see eq. (50b)
                VectorX Lt(11);
                Lt.setZero();
                Lt.head(7).noalias() += 0.5 * V3_before.transpose() * K3;
                Lt.tail(7).noalias() += 0.5 * V3.transpose() * K3;
                Lt(3) += -1;
                Lt(7) += 1;

                float_type mu_l = 0.5 * (mu_ * A_before * A_before + mu_ * A * A) / (4.0 * M_PI);
                float_type Ci = 2.0 * mu_l / (0.5 * (l_before + l));
                Ci *= dt;

                //add twist contribution to D
                for (int j=0; j<11; ++j) {
                    D(4*(i-1), 4*(i-1) + j) += Ci * Lt(0) * Lt(j);
                    D(4*(i-1) + 1, 4*(i-1) + j) += Ci * Lt(1) * Lt(j);
                    D(4*(i-1) + 2, 4*(i-1) + j) += Ci * Lt(2) * Lt(j);
                    D(4*(i-1) + 3, 4*(i-1) + j) += Ci * Lt(3) * Lt(j);
                    D(4*i, 4*(i-1) + j) += Ci * Lt(4) * Lt(j);
                    D(4*i + 1, 4*(i-1) + j) += Ci * Lt(5) * Lt(j);
                    D(4*i + 2, 4*(i-1) + j) += Ci * Lt(6) * Lt(j);
                    D(4*i + 3, 4*(i-1) + j) += Ci * Lt(7) * Lt(j);
                    D(4*i + 4, 4*(i-1) + j) += Ci * Lt(8) * Lt(j);
                    D(4*i + 5, 4*(i-1) + j) += Ci * Lt(9) * Lt(j);
                    D(4*i + 6, 4*(i-1) + j) += Ci * Lt(10) * Lt(j);
                }



                //vertex based tangent
                Vector3 t_tilde = t_before + t;
                t_tilde.normalize();


                //Lb (bend): defined for all inner vertices, see eq. (51b)
                MatrixX Lb(3, 11);
                Lb.setZero();
                for (int j=0; j<7; ++j) {
                    float_type tmp = t_tilde(0) * W3_before(0, j) + t_tilde(1) * W3_before(1, j) + t_tilde(2) * W3_before(2, j);
                    Lb(0, j) += -W3_before(0, j) + t_tilde(0) * tmp;
                    Lb(1, j) += -W3_before(1, j) + t_tilde(1) * tmp;
                    Lb(2, j) += -W3_before(2, j) + t_tilde(2) * tmp;

                    float_type tmp2 = t_tilde(0) * W3(0, j) + t_tilde(1) * W3(1, j) + t_tilde(2) * W3(2, j);
                    Lb(0, 4 + j) += W3(0,j) - t_tilde(0) * tmp2;
                    Lb(1, 4 + j) += W3(1,j) - t_tilde(1) * tmp2;
                    Lb(2, 4 + j) += W3(2,j) - t_tilde(2) * tmp2;
                }

                float_type Bi = 3.0 * mu_l / (0.5 * (l_before + l));
                Bi *= dt;

                //add bend contribution to D
                for (int j=0; j<11; ++j) {
                    for (int k=0; k<11; ++k) {
                        D(4 * (i-1) + j, 4 * (i-1) + k) += Bi * ( Lb(0, j) * Lb(0, k)
                                                                + Lb(1, j) * Lb(1, k)
                                                                + Lb(2, j) * Lb(2, k));
                    }
                }
            }



            //update mass matrix and force
            float_type a = std::sqrt(A / M_PI);
            float_type ny = M_PI * gamma_ * a;

            float_type mass;
            Vector3 f;
            if (i == 0) {
                mass = 0.5 * mass_[i];
                f = ny * t;
            } else {
                mass = 0.5 * (mass_[i - 1] + mass_[i]);
                f = ny * t - ny_before * t_before;
            }

            M(4*i, 4*i) = mass;
            M(4*i + 1, 4*i + 1) = mass;
            M(4*i + 2, 4*i + 2) = mass;
//            M(4 * i + 3, 4 * i + 3) = l * m/V * A*A/(2.0*M_PI); //see 3.10: rotational inertia has negligible effect

            //force from surface tension
            F[4*i] = f(0);
            F[4*i + 1] = f(1);
            F[4*i + 2] = f(2) - mass * gravity_; //gravity


            //update UX transformation matrix
            UX(3*i, 4*i) = 1;
            UX(3*i + 1, 4*i + 1) = 1;
            UX(3*i + 2, 4*i + 2) = 1;


            if (i == n) {
                t_last = t_before;
                ny_last = ny_before;
            }
        }


        //last vertex

        float_type mass = 0.5 * mass_[n];
        M(4*(n+1), 4*(n+1)) = mass;
        M(4*(n+1) + 1, 4*(n+1) + 1) = mass;
        M(4*(n+1) + 2, 4*(n+1) + 2) = mass;

        Vector3 f = -ny_last * t_last;
        F[4 * (n+1)] = f(0);
        F[4 * (n+1) + 1] = f(1);
        F[4 * (n+1) + 2] = f(2) - mass * gravity_;

        UX(3*(n+1), 4*(n+1)) = 1;
        UX(3*(n+1) + 1, 4*(n+1) + 1) = 1;
        UX(3*(n+1) + 2, 4*(n+1) + 2) = 1;

        char TRANS = 'n';
        long N = 4*n + 7;
        long KL = 10;
        long KU = 20;
        const long ZERO = 0;
        const long ONE = 1;
        long LDA = KL + KU + 1;
        long INFO = 0;
        float_type alpha = -1.0;
        float_type beta = dt;

        // F = -1 * D * B_vec + dt * F
        //gbmv: y = alpha * A * x + beta * y
        dgbmv_(&TRANS, &N, &N, &KL, &KU, &alpha, &D.storage()[0], &LDA, &B_vec[0], &ONE, &beta, &F[0], &ONE);

        // tmp = (-1) * U + B_vec
        //axpy: y = alpha * x + y
        std::vector<float_type> tmp(B_vec);
        daxpy_(&N, &alpha, U.data(), &ONE, &tmp[0], &ONE);

        //F = (-1) * M * tmp + 1 * F
        //gbmv: y = alpha * A * x + beta * y
        beta = 1.0;
        dgbmv_(&TRANS, &N, &N, &ZERO, &ZERO, &alpha, &M.storage()[0], &ONE, &tmp[0], &ONE, &beta, &F[0], &ONE);

        //U = 1 * B * F + 0 * U
        //gbmv: y = alpha * A * x + beta * y
        alpha = 1.0;
        beta = 0.0;
        dgbmv_(&TRANS, &N, &N, &ZERO, &ZERO, &alpha, &B_mat.storage()[0], &ONE, &F[0], &ONE, &beta, U.data(), &ONE);

        //U is the new RHS


        //construct A by looping over middle diagonal of D and adding mass
        for (int i=0; i<4*n+7; ++i) {
            D(i,i) += M(i,i);
        }

        //set D zero on boundary vertices (instead of multiplying by B, because MM is not defined for banded matrices)
        for (int i=0; i<21; ++i) {
            for (int j = 0; j < 7; ++j) {
                //boundary that is empty
                D(i, j) = 0;
                D(j, i) = 0;

                //assuming we have two floor vertices -> in my examples always the case!
                D(4 * (n + 1) + 2 - i, 4 * (n + 1) + 2 - j) = 0;
                D(4 * (n + 1) + 2 - j, 4 * (n + 1) + 2 - i) = 0;
            }
        }


        std::vector<long> ipiv(4*n + 7);

        //solver (U will contain results, not Y)
        //the matrix D is stored as if it had 20 upper diagonals. But the top 10 are not in use.
        //for the purpose of this function, I have to give it the real (used) number of upper diagonals, not including the extra space
        KU = 10;
        N = r;
        std::vector<float_type> D_before(D.storage());
        dgbsv_(&N, &KL, &KU, &ONE, &D.storage()[7 * LDA], &LDA, &ipiv[0], U.data() + 7, &N, &INFO);

        if (INFO != 0) {
//            std::cout << "Error!" << std::endl;
            throw "D is not positive definite!";
        }


        //reconstruct U after time step (stored in B_vec instead of U)
        //B_vec = B_mat * U + B_vec
        //gbmv: y = alpha * A * x + beta * y
        alpha = 1.0;
        N = 4*n + 7;
        dgbmv_(&TRANS, &N, &N, &ZERO, &ZERO, &alpha, &B_mat.storage()[0], &ONE, U.data(), &ONE, &alpha, &B_vec[0], &ONE);

        //U = B_vec
        //copy: y = x
        dcopy_(&N, &B_vec[0], &ONE, U.data(), &ONE);

        //update X
        //X = dt * UX * U + X
        //gbmv: y = alpha * A * x + beta * y
        alpha = dt;
        beta = 1.0;
        long MN = 3 * (n + 2);
        KL = 0;
        KU = n+2;
        LDA = KL + KU + 1;
        dgbmv_(&TRANS, &MN, &N, &KL, &KU, &alpha, &UX.storage()[0], &LDA, U.data(), &ONE, &beta, X.data(), &ONE);

        //end of dynamical sequence
    }


    //enforce inflow boundary condition (container)
    enforceBC();

    //treat collisions with the floor (depends on c)
    bool done = collisions();

    if (!done) {
        //don't continue if collisions are not treated yet (rollback mode)
        return;
    }

    //optionally, refine by subdivision (based on criterion f)
    meshRefinement();

    integrateContainerPos(container_velocity_, phi_velocity_, theta_velocity_);

    //Update container velocities (if required)
    //This needs to be commented out for the learning algorithm.
    //I'm leaving an example here for reference.

//    if (time_step_ > 1500) {
//        container_velocity_(0) = 0.0;
//    }

    ++time_step_;
}


void Simulation::enforceBC() {
    //reset position of top two vertices (inside container)

    float_type Uc = Qc_ / Ac_;

    Vector3 dir; //direction of nozzle
    dir << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_);
//    dir << 0, 0, -1;

    X.head(3) = container_position_ - lc_ * dir;
    X.segment(3, 3) << container_position_;

    //increase the volume of fluid of the second top-most segment
    volume_[1] += dt_ * Qc_;
    mass_[1] += rho_ * dt_ * Qc_;

    if (volume_[1] > Ac_*lc_) {
        //split segment
        float_type overhead = volume_[1] - Ac_*lc_;

        //calculate new point that is has distance lc_ to X2
        Vector3 X1 = X.segment(3, 3); //second point from top
        Vector3 X2 = X.segment(6, 3); //third point from top
        Vector3 newX = X1 + overhead / volume_[1] * (X2 - X1);

        VectorX oldX(X);
        X.resize(oldX.size() + 3);
        X.setZero();
        X.head(6) = oldX.head(6);
        X.tail(oldX.size()-6) = oldX.tail(oldX.size()-6);
        X.segment(6, 3) << newX;

        VectorX oldU(U);
        U.resize(oldU.size() + 4);
        U.setZero();
        U.head(8) = oldU.head(8);
        U.tail(oldU.size()-8) = oldU.tail(oldU.size()-8);
        U.segment(8, 4) << oldU.segment(4, 4); //new vertex has same velocity as second vertex

        volume_.push_front(volume_[0]);
        volume_[1] = overhead;
        volume_[2] -= overhead;

        mass_.push_front(mass_[0]);
        mass_[1] = rho_ * overhead;
        mass_[2] -= rho_ * overhead;

        ++n_segments_;
//        n = n_segments_ - 1;
    }
}

bool Simulation::collisions() {
    int n = n_segments_ - 1;

    //find colliding vertex closest to container/nozzle
    n_floor_vertices_ = 0;
    int last_i = n; //index that doesn't exist, if it's never updated, n_floor_vertices will evaluate to 0

    Vector3 T = X.segment(3, 3) - X.head(3);
    float_type l = T.norm();
    float_type A = volume_[0] / l;
    float_type a_before = std::sqrt(A / M_PI);

    for (int i=1; i<=n; ++i) {
        //calculate radius a of segment starting at i
        float_type a = 0;
        if (i != n+1) {
            T = X.segment(3 * (i + 1), 3) - X.segment(3 * i, 3);

            l = T.norm();
            A = volume_[i] / l;
            a = std::sqrt(A / M_PI);
        } else {
            std::cout << X << std::endl;
            writeThread("test_no_floor.dat");
        }

        if (X(3*i+2) <= 0.5 * (a + a_before)) {
            last_i = i;
            break;
        }

        a_before = a;
    }

    n_floor_vertices_ = (n+1) - last_i + 1;

    if (n_floor_vertices_ > 2) {
        //new collision

        if (collision_mode_ == cc) {
            //'capture and continue' mode
            int to_remove = n_floor_vertices_ - 2;

            for (int i=n+1; i>=last_i+2; --i) {
                //save point in pattern and remove last vertex (and segment) from simulation
                Vector2 point(X(3 * i), X(3 * i + 1)); //x and y-coordinate of third to last point

                Vector2 patternPoint = point - floor_shift_;
                pattern_.push_back(patternPoint);

                Vector3 t = X.segment(3 * i, 3) - X.segment(3 * (i - 1), 3);
                float_type r = std::sqrt(volume_[i-1] / t.norm() / M_PI);
                //TODO: this is radius of segment that ends at point,
                // should be average between segment that ends and segment that starts at point
                pattern_radius_.push_back(r);
                pattern_time_.push_back(time_step_ * dt_); //only correct for contant step size
                pattern_height_.push_back(container_position_(2));

                if (pattern_.size() == 1) {
                    //first pattern point
                    pattern_arc_length_.push_back(0.0);
                } else {
                    pattern_arc_length_.push_back(
                            pattern_arc_length_.back() + (pattern_.back() - pattern_[pattern_.size() - 2]).norm());
                }
            }

            //remove last vertex and segment
            VectorX oldX(X);
            X.resize(oldX.size() - to_remove*3);
            X = oldX.head(oldX.size() - to_remove*3);

            VectorX oldU(U);
            U.resize(oldU.size() - to_remove*4);
            U = oldU.head(oldU.size() - to_remove*4);

            volume_.resize(volume_.size() - to_remove);
            mass_.resize(mass_.size() - to_remove);

            n_segments_ -= to_remove;
            n_floor_vertices_ = 2;

        } else if (collision_mode_ == rb) {
            //'time roll-back' mode
            //TODO (see section 4.4.2)

            //predict collision time of this and future collisions
            //TODO
            std::vector<int> collisions;

            T = X.segment(3*(last_i+2), 3) - X.segment(3*(last_i+1), 3);
            l = T.norm();
            A = volume_[last_i + 1] / l;
            a_before = std::sqrt(A / M_PI);

            for (int i=last_i+2; i<n+1; ++i) {
                //calculate radius a of segment starting at i
                float_type a = 0;
                if (i != n+1) {
                    T = X.segment(3 * (i + 1), 3) - X.segment(3 * i, 3);

                    l = T.norm();
                    A = volume_[i] / l;
                    a = std::sqrt(A / M_PI);
                }

                if (X(3*i+2) <= 0.5 * (a + a_before)) {
                    //collision found

                    if (0.5 * (a + a_before) - X(3*i+2) < 1e-6) {
                        //vertex sits on floor
                    }
                }

                a_before = a;

            }

            //check if collisions sit on floor

            float_type new_dt; //TODO

            //rollback step
            X = prevX;
            U = prevU;
            mass_ = prevMass_;
            volume_ = prevVolume_;

            //rerun step with shorter timestep
            //TODO
            step(new_dt);

            //don't continue with rest of this timestep, we do every update in the timestep that was actually completed
            return false;
        }
    }

    return true;
}

void Simulation::meshRefinement() {
    int n = n_segments_ - 1;


    //lagrange polynomials (evaluated at x = 1.5) for fourth order interpolation of vertices
    const float_type l0 = -0.0625;
    const float_type l1 = 0.5625;
    const float_type l2 = 0.5625;
    const float_type l3 = -0.0625;

    //lagrange polynomials (evaluated at x = 0.75) for interpolation of segment values
    const float_type l30_0 = 0.15625;
    const float_type l31_0 = 0.9375;
    const float_type l32_0 = -0.09375;

    //lagrange polynomials (evaluated at x = 1.25) for interpolation of segment values
    const float_type l30_1 = -0.09375;
    const float_type l31_1 = 0.9375;
    const float_type l32_1 = 0.15625;

    //Lagrange polynomials over the neighbouring segments (segments 0,1,2,3,4 if I'm splitting segment 2),
    //interpolated at x = 1.75
    //For x = 2.25, it is symmetric (e.g. la_0 = la_4)
    const float_type la_0 = -0.0219727;
    const float_type la_1 = 0.205078;
    const float_type la_2 = 0.922852;
    const float_type la_3 = -0.12304675;
    const float_type la_4 = 0.017089875;


    if (subdivision_criterion_.type() != none) {
        //loop over all segments, mark those as to subdivide, where f returns true
        std::vector<unsigned int> to_divide;
        for (unsigned int i=2; i<n-1; ++i) { //can't subdivide segment 0 or 1, start at 2
            float_type l = (X.segment(3*(i+1),3) - X.segment(3*i, 3)).norm();
            if ( subdivision_criterion_(l, lc_, 0.5 * (X(3*i+2) + X(3*(i+1)+2))) ) {
                to_divide.push_back(i);
            }
        }
        if (!to_divide.empty()) {
            std::cout << "Subdividing!\n";

            VectorX oldX(X);
            X.resize(oldX.size() + 3 * to_divide.size());
            VectorX oldU(U);
            U.resize(oldU.size() + 4 * to_divide.size());
            std::deque<float_type> oldVolume(volume_);

            int lasti_old = -1;
            int lasti_new = -1;

            for (int j=0; j<to_divide.size(); ++j) {
                std::cout << "Subdividing " << to_divide[j] << std::endl;

                unsigned int iold = to_divide[j];
                unsigned int i = iold + j; //current index of element to subdivide

                //subdivide marked segment
                int nv = iold - lasti_old; //number of vertices to copy over

                X.segment(3*(lasti_new + 1), 3*nv) = oldX.segment(3*(lasti_old + 1), 3*nv);
                U.segment(4*(lasti_new + 1), 4*nv) = oldU.segment(4*(lasti_old + 1), 4*nv);

                // Interpolate position and velocity at order 4
                // TODO: Is this of order 4? Shouldn't it be using 5 points?
                Vector3 newX = l0 * oldX.segment(3*(iold-1), 3) + l1 * oldX.segment(3*(iold), 3)
                               + l2 * oldX.segment(3*(iold+1), 3) + l3 * oldX.segment(3*(iold+2), 3);

                Vector3 newU = l0 * oldU.segment(4*(iold-1), 3) + l1 * oldU.segment(4*(iold), 3)
                               + l2 * oldU.segment(4*(iold+1), 3) + l3 * oldU.segment(4*(iold+2), 3);

                X.segment(3*(lasti_new + 1 + nv), 3) << newX;
                U.segment(4*(lasti_new + 1 + nv), 3) << newU;

                //Interpolate spin at order 2
                float_type newSpin0 = l30_0 * oldU(4 * (iold-1) + 3) + l31_0 * oldU(4 * iold + 3) + l32_0 * oldU(4 * (iold+1) + 3);
                float_type newSpin1 = l30_1 * oldU(4 * (iold-1) + 3) + l31_1 * oldU(4 * iold + 3) + l32_1 * oldU(4 * (iold+1) + 3);

                U(4*i + 3) = newSpin0;
                U(4*(i+1) + 3) = newSpin1;

                //Interpolate cross-sectional area at order 4
                float_type A0 = oldVolume[iold-2] / (oldX.segment(3*(iold-1), 3) - oldX.segment(3*(iold-2), 3)).norm();
                float_type A1 = oldVolume[iold-1] / (oldX.segment(3*(iold), 3) - oldX.segment(3*(iold-1), 3)).norm();
                float_type A2 = oldVolume[iold  ] / (oldX.segment(3*(iold+1), 3) - oldX.segment(3*(iold), 3)).norm();
                float_type A3 = oldVolume[iold+1] / (oldX.segment(3*(iold+2), 3) - oldX.segment(3*(iold+1), 3)).norm();
                float_type A4 = oldVolume[iold+2] / (oldX.segment(3*(iold+3), 3) - oldX.segment(3*(iold+2), 3)).norm();

                float_type newA0 = la_0 * A0 + la_1 * A1 + la_2 * A2 + la_3 * A3 + la_4 * A4;
                float_type newA1 = la_4 * A0 + la_3 * A1 + la_2 * A2 + la_1 * A3 + la_0 * A4;

                float_type len0 = (newX - oldX.segment(3*(iold), 3)).norm();
                float_type len1 = (oldX.segment(3*(iold + 1), 3) - newX).norm();

                volume_.insert(volume_.begin() + i, newA0 * len0); //inserts before index i -> new value is at index i
                volume_[i+1] = newA1 * len1;

                //TODO: interpolate viscosity once it depends on coordinates and isn't constant (same method as spin)

                // Evenly divide mass between segments
                float_type newMass = 0.5 * mass_[i];
                mass_.insert(mass_.begin() + i, newMass);
                mass_[i+1] = newMass;

                lasti_old = iold;
                lasti_new = i + 1;
            }

            //copy over remaining values in X, U
            X.tail(oldX.size() - 3*(lasti_old + 1)) = oldX.tail(oldX.size() - 3*(lasti_old + 1));
            U.tail(oldU.size() - 4*(lasti_old + 1)) = oldU.tail(oldU.size() - 4*(lasti_old + 1));

            //print all values
            n_segments_ = mass_.size();
        }
    }
}

void Simulation::writePattern() {
    char filename[60];
    sprintf(filename, "%s_pattern_%06i.dat", date_time_string_, time_step_);

    writePattern(filename);
}

void Simulation::writePattern(std::string filename, bool isPath) {
//#ifndef ODYSSEY
//    namespace filesystem = boost::filesystem;
//#endif
    filesystem::path filePath;

    if (isPath) {
        filePath = filename;
    } else {
        filesystem::path outputDir = rootPath_ / "output";
//        if (!boost::filesystem::exists(outputDir)) {
//            throw("Directory didn't exist yet!");
//            boost::filesystem::create_directories(outputDir);
//        }

        filePath = outputDir / filename;
    }

#ifdef ODYSSEY
    std::ofstream out(filePath.str());
#else
    boost::filesystem::ofstream out(filePath);
#endif

    out << "#x y radius time nozzle_height" << std::endl;

    for (int i=0; i<pattern_.size(); ++i) {
        Vector2 point = pattern_[i];
        out << point(0) << " " << point(1) << " " << pattern_radius_[i] << " " << pattern_time_[i] << " " << pattern_height_[i] << std::endl;
    }
    out.close();
}

void Simulation::writeThread() {
    char filename[60];
    sprintf(filename, "%s_thread_%06i.dat", date_time_string_, time_step_);

    writeThread(filename);
}

void Simulation::writeThread(std::string filename, bool isPath) {

    filesystem::path filePath;

    if (isPath) {
        filePath = filename;
    } else {
        filesystem::path outputDir = rootPath_ / "output";
//        if (!boost::filesystem::exists(outputDir)) {
//            throw("Directory didn't exist yet!");
//            boost::filesystem::create_directories(outputDir);
//        }

        filePath = outputDir / filename;
    }

#ifdef ODYSSEY
    std::ofstream out(filePath.str());
#else
    boost::filesystem::ofstream out(filePath);
#endif

    out << "#x y z r(segment starting here and going down)" << std::endl;

    for (int i=0; i<n_segments_+1; ++i) {
        float_type r = 0;
        if (i < n_segments_) {
            Vector3 t = X.segment(3 * (i+1), 3) - X.segment(3 * i, 3);
            r = std::sqrt(volume_[i] / t.norm() / M_PI);
        }

        out << X(3*i) << " " << X(3*i+1) << " " << X(3*i+2) << " " << r << std::endl;
    }
    out.close();
}
void Simulation::writeU(std::string filename, bool isPath) {

    filesystem::path filePath;

    if (isPath) {
        filePath = filename;
    } else {
        filesystem::path outputDir = rootPath_ / "output";
//        if (!boost::filesystem::exists(outputDir)) {
//            throw("Directory didn't exist yet!");
//            boost::filesystem::create_directories(outputDir);
//        }

        filePath = outputDir / filename;
    }

#ifdef ODYSSEY
    std::ofstream out(filePath.str());
#else
    boost::filesystem::ofstream out(filePath);
#endif

    out << "#x y z r(segment starting here and going down)" << std::endl;

    for (int i=0; i<n_segments_+1; ++i) {
        out << U(4*i) << " " << U(4*i+1) << " " << U(4*i+2) << " " << (i < n_segments_+1 ? U(4*i+3) : 0) << std::endl;
    }
    out.close();
}

void Simulation::initContinue(const int lastStep, std::string filename) {
    time_step_ = lastStep;
}

float_type Simulation::curvature(const int s) const {
    if (s == 0 || s >= pattern_.size() - 1) {
        return 0.0;
    }

    Vector2 A;
    A << pattern_[s-1];
    Vector2 origin = pattern_[s];
    Vector2 B = pattern_[s+1];

    Vector2 a = A - origin;
    Vector2 b = B - origin;
    Vector2 c = B - A;

    float_type area = 0.5 * std::abs(a(0) * b(1) - a(1) * b(0));

    float_type k = 4.0 * area / (a.norm() * b.norm() * c.norm());

    return k;
}

void Simulation::updateFloorVelocity(Vector2 d_floor_velocity) {
    floor_velocity_ += d_floor_velocity;
}

void Simulation::updateFloorShift(Vector2 floor_velocity) {
    floor_shift_ += dt_ * floor_velocity;
}

void Simulation::updateContainerPos(Vector3 container_shift, float_type phi, float_type theta) {
    Vector3 dir_old;
    dir_old << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_);

    Vector3 pos_old = container_position_ - dir_old * lc_; //position of first vertex

    container_position_ += container_shift;
    phi_ += phi;
    theta_ += theta;

    Vector3 dir;
    dir << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_);

    Vector3 pos = container_position_ - dir * lc_;

    //first point
    u_clamp_0 = Qc_ / Ac_ * dir + (pos - pos_old) / dt_;
    //second point
    u_clamp_1 = Qc_ / Ac_ * dir + container_shift / dt_;

    //TODO: spin?

    //store in velocities -> will perform use velocities until function is called again
    container_velocity_ = container_shift / dt_;
    phi_velocity_ = phi / dt_;
    theta_velocity_ = theta / dt_;
}

void Simulation::integrateContainerPos(Vector3 container_velocity, float_type phi_velocity, float_type theta_velocity) {
    Vector3 dir_old;
    dir_old << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_);

    Vector3 pos_old = container_position_ - dir_old * lc_; //position of first vertex

    container_position_ += dt_ * container_velocity;
    phi_ += dt_ * phi_velocity;
    theta_ += dt_ * theta_velocity;

    Vector3 dir;
    dir << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_);

    Vector3 pos = container_position_ - dir * lc_;

    u_clamp_0 = Qc_ / Ac_ * dir + (pos - pos_old) / dt_;
    u_clamp_1 = Qc_ / Ac_ * dir + container_velocity;

    //TODO: spin?
    //TODO: store velocities

    container_velocity_ = container_velocity;
    phi_velocity_ = phi_velocity;
    theta_velocity_ = theta_velocity;
}


float_type Simulation::radius(unsigned int segment) const {
    Vector3 T = X.segment(3 * (segment + 1), 3) - X.segment(3 * segment, 3);
    float_type l = T.norm();
    float_type A = volume_[segment] / l;
    float_type radius = std::sqrt(A / M_PI);
    return radius;
}



filesystem::path Simulation::getOutputPath() {
    filesystem::path outputDir = rootPath_ / "output";
    return outputDir;
}
