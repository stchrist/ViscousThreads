//
// Created by Stephanie Christ on 25.10.2017.
//

#ifndef VISCOUSTHREADS_SUBDIVISIONCRITERION_H
#define VISCOUSTHREADS_SUBDIVISIONCRITERION_H


#include "global.h"



class SubdivisionCriterionFunction {
public:
    virtual bool operator()(float_type l, float_type l0, float_type z) const {
        return false;
    }
};




class SubdivisionSewingMachine : public SubdivisionCriterionFunction {
public:
    bool operator()(float_type l, float_type l0, float_type z) const {
        return l > std::min(1.96 * l0, std::pow(0.0062, std::max(0.0, (0.1 - z)/0.02)));
    }
};


//subdivide if it's almost doubled in length
class SubdivisionScalingLaw : public SubdivisionCriterionFunction {
public:
    bool operator()(float_type l, float_type l0, float_type z) const {
//        return l > std::min(1.96 * l0, std::pow(0.031, std::max(0.0, (0.2 - z)/0.02)));
        return l > 1.96 * l0;
    }
};



class SubdivisionCriterion {
public:

    explicit SubdivisionCriterion(subdivision_criterion_type type = none) : type_(type) {
        switch (type) {
            case sewingMachine:
                func = new SubdivisionSewingMachine(); break;
            case scalingLaw:
                func = new SubdivisionScalingLaw(); break;
            case none:
            default:
                func = new SubdivisionCriterionFunction(); break;
        }
    }

    bool operator()(float_type l, float_type l0, float_type z) const {
        bool res = (*func)(l, l0, z);
        return res;
    }

    subdivision_criterion_type type() const {
        return type_;
    }

private:
    subdivision_criterion_type type_;
    SubdivisionCriterionFunction *func;
};


#endif //VISCOUSTHREADS_SUBDIVISIONCRITERION_H
