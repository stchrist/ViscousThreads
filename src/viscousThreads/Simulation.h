//
// Created by Stephanie Christ on 18.10.2017.
//
// Based on B. Audoly et al.: A discrete geometric approach for simulating the dynamics of thin viscous threads, Journal of Computational Physics 253 (2013)
//

#ifndef VISCOUSTHREADS_SIMULATION_H
#define VISCOUSTHREADS_SIMULATION_H

#include <deque>
#include <list>
#ifdef ODYSSEY
    #include <filesystem/filesystem/path.h>
#else
    #include <boost/filesystem.hpp>
    namespace filesystem = boost::filesystem;
#endif

#include "global.h"
#include "SubdivisionCriterion.h"

class Simulation {
public:
    Simulation();
    Simulation(const float_type dt, const float_type lc, const float_type h, const Vector3 container_velocity);

    void run();

    void step(const float_type dt);
    void step_sparse(const float_type dt);
    void step_lapack(const float_type dt);

    void writePattern();
    void writePattern(std::string filename, bool isPath = false);
    void writeThread();
    void writeThread(std::string filename, bool isPath = false);
    void writeU(std::string filename, bool isPath = false);
    filesystem::path getRootPath() { return rootPath_; }

    void initContinue(const int lastStep, std::string filename);

    float_type curvature(const int s) const;
    float_type arcLength(const int s) const { return pattern_arc_length_[s]; }

    unsigned int getTimeStep() const { return time_step_; }
    float_type getDt() const { return dt_; }

    void updateFloorShift(Vector2 floor_velocity);
    void updateFloorVelocity(Vector2 d_floor_velocity);
    void updateContainerPos(Vector3 container_shift, float_type phi, float_type theta);
    void integrateContainerPos(Vector3 container_velocity, float_type phi_velocity, float_type theta_velocity);
    //TODO: functions to update velocity (instead of setting it)

    unsigned int nSegments() const { return n_segments_; }
    float_type radius(unsigned int segment) const;
    unsigned int getPatternSize() const { return pattern_.size(); }
    Vector2 getPattern(int i) const { return pattern_[i]; }
    VectorX getThread() const { return X; }
    Vector3 containerPos() const { return container_position_; }
    Vector3 containerVel() const { return container_velocity_; }
    float_type phi() const { return phi_; }
    float_type phiVel() const { return phi_velocity_; }
    float_type theta() const { return theta_; }
    float_type thetaVel() const { return theta_velocity_; }

    filesystem::path getOutputPath();
    char* getDateTimeString() { return date_time_string_; }

private:
    enum collision_mode {cc, rb};

    void enforceBC();
    bool collisions();
    void meshRefinement();

    const int n_steps_;                     ///< number of time steps to simulate
    const float_type total_time_;           ///< total time to simulate (= n_steps_ * dt_)
    const float_type dt_;                   ///< time step size
    int time_step_;                         ///< current time step (= number of time steps that have been completed)
    int simulation_time_;                   ///< current simulation time (time_step_ * dt_ if without rollback)
    const float_type lc_;                   ///< mesh size

    const collision_mode collision_mode_;   ///< collision mode describing how to handle collision with floor
    const SubdivisionCriterion subdivision_criterion_; ///< criterion according to which the mesh is refined

    VectorX X;                              ///< vector of all positions (x0, y0, z0, x1, y1, z1, ...)
    VectorX U;                              ///< vector of all velocities (ux, uy, uz, v)
    std::deque<float_type> volume_;         ///< fluid volume in thread segments
    std::deque<float_type> mass_;           ///< fluid mass in thread segments

    unsigned int n_segments_;               ///< number of segments
    unsigned int n_floor_vertices_;         ///< number of vertices touching floor


    VectorX prevX;
    VectorX prevU;
    std::deque<float_type> prevVolume_;
    std::deque<float_type> prevMass_;


    /** material and flow constants */
    const float_type rho_;                  ///< density of fluid
    const float_type mu_;                   ///< dynamic viscosity of fluid
    const float_type gamma_;                ///< surface tension of fluid
    const float_type Qc_;                   ///< volume rate of fluid
    const float_type Ac_;                   ///< container diameter
    float_type h_;                          ///< container height over floor (can be changed by learning agent)
    const float_type gravity_;              ///< gravity

    /** boundary conditions */
    Vector2 floor_shift_;                   ///< amount by which floor has been shifted
    Vector2 floor_velocity_;                ///< velocity of the floor (can be changed by learning agent)
    Vector3 container_position_;            ///< position of container/nozzle
    Vector3 container_velocity_;            ///< velocity of container/nozzle
    float_type phi_;                        ///< azimuthal angle of nozzle direction (measured from x-axis)
    float_type phi_velocity_;
    float_type theta_;                      ///< polar/inclination angle of nozzle direction (inclination from z-direction)
    float_type theta_velocity_;

    Vector3 u_clamp_0;                      ///< boundary velocity for first vertex, same as second, but including change of angle of nozzle (can be changed by learning agent)
    Vector3 u_clamp_1;                      ///< boundary velocity for second vertex (can be changed by learning agent)
    const float_type v_clamp_1;             ///< boundary rotation between first two vertices
    Vector3 u_clamp_2;                      ///< boundary velocity for last two vertices (can be changed by learning agent)
    const float_type v_clamp_2;             ///< boundary rotation for last two vertices

    /** resulting pattern */
    std::vector<Vector2> pattern_;            ///< (x,y) coordinates of points in final pattern (no z-coordinate, since it is on the floor)
    std::vector<float_type> pattern_radius_;  ///< thickness/radius at points in final pattern
    std::vector<float_type> pattern_time_;    ///< time at which the pattern has been written -> for stable coiling, time difference between writing and collision is constant, so this is good enough for calculation of coiling frequency
    std::vector<float_type> pattern_arc_length_;    ///< arc length for each point in pattern
    std::vector<float_type> pattern_height_;  ///< height from which fluid was falling when pattern point was deposited

    const bool file_output_;                ///< flag whether to write files
    const unsigned int output_freq_;        ///< file output frequency

    const filesystem::path rootPath_;         ///< path to root of project

    char date_time_string_[30];               ///< string with date and time for output filename
};


#endif //VISCOUSTHREADS_SIMULATION_H
