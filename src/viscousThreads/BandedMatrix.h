//
// Created by Stephanie Christ on 13.11.2017.
//

#ifndef VISCOUSTHREADS_BANDEDMATRIX_H
#define VISCOUSTHREADS_BANDEDMATRIX_H


#include "global.h"

#include <vector>


class BandedMatrix {
public:
    BandedMatrix(unsigned int rows, unsigned int cols, unsigned int kl, unsigned int ku)
            : rows_(rows)
            , cols_(cols)
            , kl_(kl)
            , ku_(ku)
            , storage_((kl + ku + 1) * cols, 0.0)
    {}

//    inline float_type operator()(int i, int j) const {
//        int r = ku_ + 1 + i;
//        int c = j;
//
//        if (r < 0 || r >= kl_ + ku_ + 1) {
//            return 0.0;
//        }
//
//        return storage_[r + c * (kl_ + ku_ + 1)];
//    }

    inline float_type& operator()(int i, int j) {
        int r = ku_ + i - j;
        int c = j;

        return storage_[r + c * (kl_ + ku_ + 1)];
    }

    friend std::ostream& operator<<(std::ostream& stream, const BandedMatrix& matrix) {
        for (int i=0; i<matrix.rows_; ++i) {
            for (int j=0; j<matrix.cols_; ++j) {
                int r = matrix.ku_ + i - j;
                int c = j;
                if (r < 0 || r >= matrix.kl_ + matrix.ku_ + 1) {
                    std::cout << 0.0 << " ";
                } else {
                    std::cout << matrix.storage_[r + c * (matrix.kl_ + matrix.ku_ + 1)] << " ";
                }
            }
            std::cout << std::endl;
        }
//        std::cout << std::endl << std::endl << std::endl;

        return stream;
    }

    std::string structure(std::ostream& stream, const BandedMatrix& matrix) {
        for (int i=0; i<matrix.rows_; ++i) {
            for (int j=0; j<matrix.cols_; ++j) {
                int r = matrix.ku_ + i - j;
                int c = j;
                if (r < 0 || r >= matrix.kl_ + matrix.ku_ + 1 || matrix.storage_[r + c * (matrix.kl_ + matrix.ku_ + 1)] == 0) {
                    std::cout << " ";
                } else {
                    std::cout << "x";
                }
            }
            std::cout << std::endl;
        }
        std::cout << std::endl << std::endl << std::endl;

    }


    std::vector<float_type> storage() { return storage_; }
    void setValues(std::vector<float_type> newStorage) {
        storage_ = std::vector<float_type>(newStorage);
    }

    std::vector<float_type> storage_;

    const int rows_;
    const int cols_;
    const int kl_;
    const int ku_;
};



class BandedMatrixExtraSpace : public BandedMatrix {
public:
    BandedMatrixExtraSpace(unsigned int rows, unsigned int cols, unsigned int kl, unsigned int ku)
            : BandedMatrix(rows, cols, kl, ku) {
        storage_.resize((kl + ku + 1 + kl) * cols, 0.0);
    }

//    inline float_type operator()(int i, int j) const {
//        int r = ku_ + 1 + i;
//        int c = j;
//
//        if (r < 0 || r >= kl_ + ku_ + 1 + kl_) {
//            return 0.0;
//        }
//
//        return storage_[r + c * (kl_ + ku_ + 1 + kl_)];
//    }

    inline float_type& operator()(int i, int j) {
        int r = ku_ + i - j;
        int c = j;

        return storage_[r + kl_ + c * (kl_ + ku_ + 1 + kl_)];
    }
};


#endif //VISCOUSTHREADS_BANDEDMATRIX_H
