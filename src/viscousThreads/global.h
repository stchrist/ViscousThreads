//
// Created by Stephanie Christ on 25.10.2017.
//

#ifndef VISCOUSTHREADS_GLOBAL_H
#define VISCOUSTHREADS_GLOBAL_H

#include <Eigen/Dense>

typedef double float_type;


typedef Eigen::Matrix<float_type, 2, 1> Vector2;                            ///< vector of size 2 with templated type
typedef Eigen::Matrix<float_type, 3, 1> Vector3;                            ///< vector of size 3 with templated type
typedef Eigen::Matrix<float_type, Eigen::Dynamic, 1> VectorX;               ///< vector with dynamic size and templated type
typedef Eigen::Matrix<float_type, Eigen::Dynamic, Eigen::Dynamic> MatrixX;  ///< matrix with dynamic size and templated type


enum subdivision_criterion_type {
    none,
    sewingMachine,
    scalingLaw
};

#endif //VISCOUSTHREADS_GLOBAL_H
