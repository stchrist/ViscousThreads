cmake_minimum_required(VERSION 2.8)
project(viscousThreads)

#set(CMAKE_CXX_STANDARD 11)

add_subdirectory(third_party)
add_subdirectory(viscousThreads)
add_subdirectory(simulation)
add_subdirectory(smartiesApp)
