#filename = "2018-02-21_09:57:12_pattern_039000.dat"
filename = "continuation_h16.1/2018-02-26_10:14:13_pattern_007000.dat"
set term pngcairo size 1280,960 font 'Helvetica,20'
set output "validation-height-radius.png"
#stats filename using ($5/25.36) prefix "X" nooutput
set logscale xy
set xlabel "H/L*"
set ylabel "R/L*"
set xrange [0.12:1.2]
set yrange [0.009:0.050]
set xtics (0.15, 0.20, 0.30, 0.50, 0.70, 1.00)
set ytics (0.010, 0.015, 0.020, 0.030)
#set key left top
plot filename using ($5/25.36):(sqrt($1*$1+$2*$2)/25.36) with lines lw 3 lc rgb "#b09a5f" title "Increasing height", \
"ribe-reference-ordered.dat" using 1:2 with lines lt -1 lw 2 title "Reference solution" 
