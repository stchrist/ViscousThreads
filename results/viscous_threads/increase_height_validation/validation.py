import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

filename = sys.argv[1]
data = np.loadtxt(filename)

data_reference = np.loadtxt('ribe-reference-ordered.dat')


x = data[:,0]
y = data[:,1]
h = data[:,4]


def set_style():
	#plt.style.use(['seaborn-white', 'seaborn-paper'])
	plt.style.use(['seaborn-paper'])
	plt.rcParams["font.family"] = "Times", "Times New Roman"




set_style()


fig, ax = plt.subplots()

ax.loglog(h/25.36, np.sqrt(x*x + y*y)/25.36, color='#b09a5f', linewidth=1.5)
ax.loglog(data_reference[:,0], data_reference[:,1], color='black')


# Set aspect ratio
ax.set_aspect('equal')

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# Set axis label and location
ax.set_xlabel(r'$\frac{H}{L^*}$', rotation=0, fontsize=24)
ax.xaxis.set_label_coords(0.95,0.19)
ax.set_ylabel(r'$\frac{R}{L^*}$', rotation=0, fontsize=24)
ax.yaxis.set_label_coords(0.06,0.84)

# Set axis limits
ax.set_xlim(0.12,1.2)
ax.set_ylim(0.009,0.050)

# Set axis ticks
ax.minorticks_off()
ax.set_xticks([0.15, 0.20, 0.30, 0.50, 0.70, 1.00])
ax.set_xticklabels(['0.15', '0.20', '0.30', '0.50', '0.70', '1.00'])
ax.xaxis.set_tick_params(labelsize=16)
ax.set_yticks([0.010, 0.015, 0.020, 0.030])
ax.set_yticklabels(['0.010', '0.015', '0.020', '0.030'])
ax.yaxis.set_tick_params(labelsize=16)


plt.tight_layout()

plt.savefig('validation_python.pdf')

plt.show()

