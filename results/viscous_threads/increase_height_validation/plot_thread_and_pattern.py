import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import norm

filename = sys.argv[1]
data = np.loadtxt(filename)

filename2 = filename.replace('thread', 'pattern')
data2 = np.loadtxt('2018-02-21_09:57:12_pattern_039000.dat')

data2_firstidx = []

if len(sys.argv) > 2:
    firstidx = sys.argv[2]
    data2_firstidx = np.loadtxt(filename2[:-1*(len(firstidx)+4)] + firstidx + filename2[-4:])

data2 = data2[14500:,:3]
#data = np.concatenate((data[:,:3], data2[::-1,:]))

colorgradient = True

x = data[:,0]
y = data[:,1]
z = data[:,2]


def set_style():
	#plt.style.use(['seaborn-white', 'seaborn-paper'])
	#plt.style.use(['seaborn-paper'])
	plt.rcParams["font.family"] = "Times", "Times New Roman"
	plt.rcParams["figure.figsize"] = 3,10
	#plt.rcParams["grid.linewidth"] = 1

set_style()



def plot_cylinder(p0, p1, R, col):
	#vector in direction of axis
	v = p1 - p0
	#find magnitude of vector
	mag = norm(v)
	#unit vector in direction of axis
	v = v / mag
	#make some vector not in the same direction as v
	not_v = np.array([1, 0, 0])
	if (v == not_v).all():
	    not_v = np.array([0, 1, 0])
	#make vector perpendicular to v
	n1 = np.cross(v, not_v)
	#normalize n1
	n1 /= norm(n1)
	#make unit vector perpendicular to v and n1
	n2 = np.cross(v, n1)
	#surface ranges over t from 0 to length of axis and 0 to 2*pi
	t = np.linspace(0, mag, 100)
	theta = np.linspace(0, 2 * np.pi, 100)
	#use meshgrid to make 2d arrays
	t, theta = np.meshgrid(t, theta)
	#generate coordinates for surface
	X, Y, Z = [p0[i] + v[i] * t + R * np.sin(theta) * n1[i] + R * np.cos(theta) * n2[i] for i in [0, 1, 2]]
	#print X,Y,Z
	ax.plot_surface(X, Y, Z, color=col, edgecolors=col)


def plot_thread(x ,y, z, r, col):
	for i in xrange(len(x)-1):
		p0 = np.array([x[i],y[i],z[i]])
		p1 = np.array([x[i+1],y[i+1],z[i+1]])
		R = r[i]
		#print p0, p1, R
		plot_cylinder(p0, p1, R, col)



fig = plt.figure()
ax = plt.axes(projection='3d')

#ax.scatter(x, y, z, zorder=3) #, s=5)
#ax.plot3D(x, y, z, linewidth=10, solid_capstyle='round') #zorder=2)
plot_thread(x, y, z, data[:,3], plt.cm.jet(0))
#ax.scatter(data3[:,0], data3[:,1], data3[:,2], zorder=1)
#ax.plot3D(data2[:,0], data2[:,1], np.zeros(len(data2)), zorder=1)
if not colorgradient:
    ax.plot3D(data2[:,0], data2[:,1], np.zeros(len(data2)), zorder=1, color=plt.cm.jet(0))
else:
    N = len(data2[:,0])
    for i in range(N-1):
        ax.plot3D(data2[i:i+2,0], data2[i:i+2,1], color=plt.cm.jet(255*i/(N-1)), linewidth=1, zorder=1)

data = np.loadtxt('2018-02-21_09:57:12_thread_039000.dat')
x = data[:,0]
y = data[:,1]
z = data[:,2]
#ax.plot3D(x, y, z, linewidth=10, solid_capstyle='round', color=plt.cm.jet(255))
plot_thread(x, y, z, data[:,3], plt.cm.jet(255))
#N = len(z)
#normalize_factor = max(z) - min(z)
#for i in xrange(N-1):
#    ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], color=plt.cm.jet(255*i/(N-1)))


# Set axis limits and aspect ration
# Create cubic bounding box to simulate equal aspect ratio
#max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max()
#mid_x = (x.max()+x.min()) * 0.5
#mid_y = (y.max()+y.min()) * 0.5
#mid_z = (z.max()+z.min()) * 0.5
#ax.set_xlim(mid_x - max_range, mid_x + max_range)
#ax.set_ylim(mid_y - max_range, mid_y + max_range)
#ax.set_zlim(mid_z - max_range, mid_z + max_range)
ax.set_xlim(-1.5, 1.5)
ax.set_ylim(-1.5, 1.5)
#ax.set_zlim(0.025,1.3)


# Set axis label and location
ax.set_xlabel('x', fontsize=20)
#ax.xaxis.set_label_coords(0.96,0.15)
ax.set_ylabel('y', fontsize=20)
#ax.yaxis.set_label_coords(-0.2,0.48)
ax.set_zlabel('z', rotation=0, fontsize=20, verticalalignment='top')


# Set axis ticks
#ax.minorticks_off()
ax.set_xticks([0.0, 1.0])
#ax.set_xticklabels(['0.15', '0.20', '0.30', '0.50', '0.70', '1.00'])
ax.xaxis.set_tick_params(labelsize=16)
ax.set_yticks([0.0, -1.0])
ax.set_yticklabels(['0', '1']) # ugly hack so that it looks like axes are oriented in a more sensible way -> doesn't make a difference for the result, they mean nothing
ax.yaxis.set_tick_params(labelsize=16)
#ax.set_zticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2])
ax.zaxis.set_tick_params(labelsize=16)


[t.set_va('center') for t in ax.get_yticklabels()]
[t.set_ha('left') for t in ax.get_yticklabels()]
[t.set_va('center') for t in ax.get_xticklabels()]
[t.set_ha('right') for t in ax.get_xticklabels()]
[t.set_va('center') for t in ax.get_zticklabels()]
#[t.set_ha('left') for t in ax.get_zticklabels()]


ax.grid(False)
ax.xaxis.pane.set_edgecolor('black')
ax.yaxis.pane.set_edgecolor('black')
ax.zaxis.pane.set_edgecolor('black')
ax.xaxis.pane.fill = False
ax.yaxis.pane.fill = False
ax.zaxis.pane.fill = False


ax.xaxis._axinfo['tick']['inward_factor'] = 0
ax.xaxis._axinfo['tick']['outward_factor'] = 0.4
ax.yaxis._axinfo['tick']['inward_factor'] = 0
ax.yaxis._axinfo['tick']['outward_factor'] = 0.4
ax.zaxis._axinfo['tick']['inward_factor'] = 0
ax.zaxis._axinfo['tick']['outward_factor'] = 0.4
ax.zaxis._axinfo['tick']['outward_factor'] = 0.4


#ax.set_zlim(0, 1.5)
#ax.set_aspect('equal')
ax.view_init(elev=5., azim=225)
ax.view_init(elev=5., azim=135)


plt.tight_layout()

plt.show()
#plt.plot(x, y, z);
