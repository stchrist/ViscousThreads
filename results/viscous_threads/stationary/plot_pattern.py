import sys
import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]
data = np.loadtxt(filename)

colorgradient = False
#colorgradient = True

startidx = 0
#startidx = 14500
x = data[startidx:,0]
y = data[startidx:,1]
#r = data[startidx:,2]


def set_style():
	#plt.style.use(['seaborn-white', 'seaborn-paper'])
	plt.style.use(['seaborn-paper'])
	plt.rcParams["font.family"] = "Times", "Times New Roman"




set_style()


fig, ax = plt.subplots()

if not colorgradient:
    ax.plot(x, y, linewidth=1)
else:
    N = len(y)
    for i in range(N-1):
        ax.plot(x[i:i+2], y[i:i+2], color=plt.cm.jet(255*i/(N-1)))



#minr = min(data[:,2])
#maxr = max(data[:,2])
#minr = 0.041
#maxr = 0.0475
#
#plt.figure()
#N = len(y)
#for i in range(N-1):
#    colorval = 255 * (r[i] - minr) / (maxr - minr)
#    plt.plot(y[i:i+2], x[i:i+2], color=plt.cm.jet(int(colorval)))




# Add arrow showing direction (only occasionally necessary)
#arrowidx = 1560
#plt.arrow(x[arrowidx], y[arrowidx], x[arrowidx+1] - x[arrowidx], y[arrowidx+1] - y[arrowidx], shape='full', lw=0, length_includes_head=True, head_width=.06)



# Set aspect ratio
ax.set_aspect('equal')

ax.axhline(0, color='black', linewidth=1)
ax.axvline(0, color='black', linewidth=1)


# Set axis limits
ax.set_xlim(-0.35,0.35)
ax.set_ylim(-0.35,0.35)

# Set axis label and location
ax.set_xlabel('x', fontsize=20)
#ax.xaxis.set_label_coords(0.96,0.15)
ax.set_ylabel('y', rotation=0, fontsize=20)
ax.yaxis.set_label_coords(-0.2,0.48)


# Set axis ticks
#ax.minorticks_off()
#ax.set_xticks([0.15, 0.20, 0.30, 0.50, 0.70, 1.00])
#ax.set_xticklabels(['0.15', '0.20', '0.30', '0.50', '0.70', '1.00'])
ax.xaxis.set_tick_params(labelsize=16)
#ax.set_yticks([0.010, 0.015, 0.020, 0.030])
#ax.set_yticklabels(['0.010', '0.015', '0.020', '0.030'])
ax.yaxis.set_tick_params(labelsize=16)




plt.tight_layout()


plt.savefig(filename[:-4] + '.pdf', bbox_inches='tight')
#plt.savefig(filename[:-4] + '_col-radius.png')
#plt.savefig(filename[:-4] + '_color-radius.svg', format='svg', dpi=1200)
plt.show()

