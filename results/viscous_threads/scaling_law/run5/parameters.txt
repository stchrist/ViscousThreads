Simulation::Simulation() : n_steps_(50000), dt_(0.001), total_time_(n_steps_ * dt_), time_step_(0), lc_(0.2),
                           collision_mode_(cc), subdivision_criterion_(scalingLaw),
                           rho_(1), mu_(1000), gamma_(1.0), Qc_(0.56), Ac_(0.32), h_(10), gravity_(981),
                           floor_shift_(Vector2().setZero()), floor_velocity_((Vector2() << 0.0, 0.0).finished()),
                           container_position_((Vector3() << 0.0, 0.0, h_).finished()),
                           container_velocity_((Vector3() << 0.0, 0.0, 0.0).finished()),
                           u_clamp_1((Vector3() << 0.0, 0.0, -Qc_ / Ac_).finished()),
                           v_clamp_1(0.0),
                           u_clamp_2((Vector3() << floor_velocity_(0), floor_velocity_(1), 0.0).finished()),
                           v_clamp_2(0.0),
                           file_output_(true), output_freq_(1000), rootPath_(getProjectRootDir())

Coiling frequence =  1.79498185507
Radius =  0.132816661538



H(g/nu^2)^(1/3) = 0.99 -> complex regime
