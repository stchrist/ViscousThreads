Simulation::Simulation() : n_steps_(50000), dt_(0.001), total_time_(n_steps_ * dt_), time_step_(0), lc_(0.2),
                           collision_mode_(cc), subdivision_criterion_(scalingLaw),
                           rho_(1), mu_(50000), gamma_(1.0), Qc_(0.8), Ac_(0.32), h_(10), gravity_(981),
                           floor_shift_(Vector2().setZero()), floor_velocity_((Vector2() << 0.0, 0.0).finished()),
                           container_position_((Vector3() << 0.0, 0.0, h_).finished()),
                           container_velocity_((Vector3() << 0.0, 0.0, 0.0).finished()),
                           phi_(0.0), theta_(M_PI),
                           u_clamp_1(-Qc_ / Ac_ * (Vector3() << std::sin(theta_) * std::cos(phi_), std::sin(theta_) * std::sin(phi_), std::cos(theta_)).finished()),
                           v_clamp_1(0.0),
                           u_clamp_2((Vector3() << floor_velocity_(0), floor_velocity_(1), 0.0).finished()),
                           v_clamp_2(0.0),
                           file_output_(true), output_freq_(1000), rootPath_(getProjectRootDir())


Coiling frequence =  0.276891432188
Radius =  0.311709073852


H(g/nu^2)^(1/3) = 0.073 -> viscous coiling


2018-02-13_15:23:45

