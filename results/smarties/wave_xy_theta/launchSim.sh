# This file needs to contain one uncommented line in the following format:

# ../viscousThreadsApp $1 ../patterns/PATTERN_FILE (PATTERN SCALE FACTOR) (PATTERN OUTPUT INTERVAL)

# PATTERN SCALE FACTOR: Every point in the target pattern gets multiplied by this scaling factor.
#                       This helps get the pattern to a scale which matches the simulation scale.
#                       Default value: 1.0
# PATTERN OUTPUT INTERVAL: Define how often you want to write the output pattern to a file. 0 if never.
#                          Default value: 0

#../viscousThreadsApp $1 ../patterns/line.coord 5.0 xy_theta 1000
../viscousThreadsApp $1 ../patterns/simple-wave.coord 0.035 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/St.coord 0.05 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/Stephanie.coord 0.022 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/Mahadevan.coord 0.03 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/Brentwood.coord 0.045 xy_theta 1000
#../viscousThreadsApp $1 ../patterns/cat.coord 0.063 xy_theta 1000
