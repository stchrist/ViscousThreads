//
//  main.cpp
//  viscous-threads
//
//  Created by Stephanie Christ on 10/04/18.
//  Copyright (c) 2018 Stephanie Christ. All rights reserved.
//

#include "Communicators/Communicator.h"
#include "Simulation.h"
#include <fstream>
#include <vector>

//float_type MIN_HEIGHT = 0.5;
float_type MIN_HEIGHT = 0.05;
float_type MAX_HEIGHT = 5.0;
float_type MIN_THETA = M_PI / 2.0;
float_type MAX_THETA = 3.0 * M_PI / 2.0;
float_type MAX_VEL = 2.0;


class ViscousThreadsEnvironment {
public:
    const int nsteps = 20; //number of steps to run in every advance()
    int info = 1, step = 0;

    enum StateType {
        last_point_s,
        reduced_s
    };
    StateType stateType_ = reduced_s;

    enum ActionType {
        full_a,
        reduced_a
    };
    ActionType actionType_ = reduced_a;

    ViscousThreadsEnvironment(std::string patternFile, double patternScale, int nOutput, float_type init_lc, float_type init_h, Vector3 init_v)
    : simStabilized(init_lc, init_h, init_v)
    , lastIdx_(0)
    , patternIdx_(0)
    , stabilizedLastIdx(0)
    , outputIdx(0)
    , everyNoutput(nOutput)
    , simulationError_(false)
    {
        //load pattern
        std::ifstream file(patternFile);

        //scale: what to multiply positions in goal pattern with (or would division make more sense conceptually?)
        double scale = patternScale;

//    std::vector<std::pair<int,int>> pattern;

        //file contains one line of x,y,x,y,x,y
        std::string x, y;
        Vector2 p0, p1, p2, p_start;

        int idx = 0;
        float_type dist = 0.0;
        while (std::getline(file, x, ',') && std::getline(file, y, ',')) {
            ++idx;

            p0 = p1;
            p1 = p2;
            p2 << stof(x), stof(y);

            p2 *= scale;

            if (idx == 1) {
                p_start = p2;
            }

//        pattern.push_back(std::make_pair(stoi(x), stoi(y)));

            patternPoints_.push_back(p2 - p_start);

            if (idx > 2) {
                //we've had at least three points -> can calculate curvature
                Vector2 a = p0 - p1;
                Vector2 b = p2 - p1;
                Vector2 c = p2 - p0;

                dist += a.norm();

                double area = 0.5 * std::abs(a(0) * b(1) - a(1) * b(0));
                double k = 4.0 * area / (a.norm() * b.norm() * c.norm());

                pattern_.push_back(std::make_pair(dist, k));
            } else if (idx == 2) {
                pattern_.push_back(std::make_pair(0, 0));
            }
        }

        pattern_.push_back(std::make_pair(dist + (p2 - p1).norm(), 0));


        //stabilize simulation
        for (int i=0; i<1500; ++i) { //TODO: stabilize for longer, use enough time steps so that at most one point per step gets deposited
            simStabilized.step_lapack(simStabilized.getDt());
//            if (!(i%100)) {
//                simStabilized.writePattern();
//                std::cout << "Step " << i << " done!" << std::endl;
////            simStabilized.writePattern();
////            simStabilized.writeThread();
//            }
        }

        std::string dateTimeString = simStabilized.getDateTimeString();
        char filename[60];
        sprintf(filename, "../output_patterns/%s_pattern_stabilized.dat", dateTimeString.c_str());
        simStabilized.writePattern(filename, true);


        Vector3 zeros;
        zeros.setZero();
        simStabilized.integrateContainerPos(zeros, 0.0, 0.0);

        if (simStabilized.getPatternSize() > 0) {
            zero_arc_length_ = simStabilized.arcLength(simStabilized.getPatternSize() - 1);
            stabilizedLastIdx = simStabilized.getPatternSize() - 1;
        }

        std::cout << "Stabilized! " << stabilizedLastIdx << std::endl;
    }

    void reset(std::mt19937 &gen) {
        sim = new Simulation(simStabilized); //TODO: check if stabilization works!
        lastIdx_ = stabilizedLastIdx;
        patternIdx_ = 0;
        simulationError_ = false;
        currentState = getState();
    }

    bool is_over() {
        if (sim->theta() < MIN_THETA || sim->theta() > MAX_THETA) {
            return true;
        }

        Vector3 containerPos = sim->containerPos();
        if (containerPos(2) < MIN_HEIGHT || containerPos(2) > MAX_HEIGHT) {
            return true;
        }

        float_type arc_length = sim->arcLength(sim->getPatternSize() - 2);

        if (arc_length - zero_arc_length_ > pattern_.back().first) {
            return true;
        }

        return sim->getTimeStep() > 10000;
    }

    int advance(std::vector<double> action) {
        simulationError_ = false;

        switch (actionType_) {
            case full_a:
                takeActionFull(action);
                break;
            case reduced_a:
                takeActionReduced(action);
                break;
        }

        for (int i=0; i<nsteps; ++i) {
            try {
                sim->step_lapack(sim->getDt());
            } catch (...) {
                //threw error in step_lapack -> must be because matrix is not positive definite!
                simulationError_ = true;
                return 1;
            }
            if (is_over()) return 1;
        }
        return 0;
    }

    int stateDimension() {
        switch (stateType_) {
            case last_point_s:
                return 12;
            case reduced_s:
                return 5;
        }
    }
    
    std::vector<double> getState() {
        std::vector<double> state;
        switch (stateType_) {
            case last_point_s:
                state = getStateLastPoint();
            case reduced_s:
                state = getStateReduced();
        }

        currentState = state;
//        for (auto i: state) {
//              std::cout << i << " ";
//        }
//        std::cout << std::endl;

        return state;
    }

    int actionDimension() {
        switch (actionType_) {
            case full_a:
                return 5;
            case reduced_a:
                return 2;
        }
    }

    double getReward() {
//        return getRewardAll();
//        return getRewardNormalized();
//        return getRewardPosition();
        return getRewardPositionIntegrated();
    }

    void writePattern() {
//        std::cout << outputIdx << std::endl;
        if (!(outputIdx % everyNoutput)) {
            std::string dateTimeString = sim->getDateTimeString();
            char filename[60];
            sprintf(filename, "../output_patterns/%s_pattern%06i.dat", dateTimeString.c_str(), outputIdx);
            sim->writePattern(filename, true);
        }
        ++outputIdx;
    }

private:
    Simulation *sim;
    Simulation simStabilized;

    std::vector<std::pair<double, double>> pattern_; //target pattern
    std::vector<Vector2> patternPoints_;

    int stabilizedLastIdx;
    double zero_arc_length_;
    unsigned int lastIdx_;
    int patternIdx_;
    int outputIdx;
    int everyNoutput;

    bool simulationError_; //true if we got an error in the simulation (used to know which reward to give)
    std::vector<double> currentState;


    std::vector<double> getStateLastPoint() {
        std::vector<double> state;

        if (sim->getPatternSize() < 3) {
            state.resize(stateDimension(), 0.0);
            return state;
        }



        double arc_length = sim->arcLength(sim->getPatternSize() - 2) - zero_arc_length_;

        while (patternIdx_ < (int)pattern_.size() - 1 && arc_length > pattern_[patternIdx_ + 1].first) {
            ++patternIdx_;
        }

        float_type x0 = pattern_[patternIdx_].first;
        float_type x1 = pattern_[patternIdx_ + 1].first;

        //linearly interpolate curvature between known points
        double goal_curvature = ((x1 - arc_length) * pattern_[patternIdx_].second + (arc_length - x0) * pattern_[patternIdx_ + 1].second)
                                / (x1 - x0);

        //TODO: is that correct point to use for curvature?
        double curvature = sim->curvature(sim->getPatternSize() - 2); //curvature at point that has both neighbours on floor

        state.push_back(goal_curvature);
        state.push_back(curvature);


        //TODO: add current container position/movement to state -> What exactly???
        VectorX containerPos = sim->containerPos();
        state.push_back(containerPos(0));
        state.push_back(containerPos(1));
        state.push_back((containerPos(2) - MIN_HEIGHT) / (MAX_HEIGHT - MIN_HEIGHT));
        state.push_back(fmod(sim->phi(), (2.0*M_PI)) / 2.0*M_PI);
        state.push_back((sim->theta() - MIN_THETA) / (MAX_THETA - MIN_THETA));

        //this corresponds to last action taken
        VectorX containerVel = sim->containerVel();
        state.push_back(containerVel(0) / MAX_VEL);
        state.push_back(containerVel(1) / MAX_VEL);
        state.push_back(containerVel(2) / MAX_VEL);
        state.push_back(sim->phiVel() / MAX_VEL);
        state.push_back(sim->thetaVel() / MAX_VEL);

//    Vector2 patternPos = sim->getPattern(sim->getPatternSize() - 2);
//    state.push_back(patternPos(0));
//    state.push_back(patternPos(1));
        if (simulationError_) {
            std::vector<double> newstate = currentState;
            for (int i=7; i<12; ++i) {
                newstate[i] = state[i];
            }
            return newstate;
        }

        //TODO: do I need combinations of features?

        return state;
    }

    std::vector<double> getStateReduced() {
        std::vector<double> state;

        //not enough data to make a reasonable state
        if (sim->getPatternSize() < 3) {
            state.resize(stateDimension(), 0.0);
            return state;
        }


        float_type arc_length = sim->arcLength(sim->getPatternSize()) - zero_arc_length_;

        while (patternIdx_ < (int)pattern_.size() - 1 && arc_length > pattern_[patternIdx_ + 1].first) {
            ++patternIdx_;
        }

        state.push_back(arc_length);



        //add current container position/movement to state -> What exactly???
        state.push_back(sim->containerPos()(0));
        state.push_back(sim->containerPos()(1));

        //this corresponds to last action taken
        state.push_back(sim->containerVel()(0));
        state.push_back(sim->containerVel()(1));

//    Vector2 patternPos = sim->getPattern(sim->getPatternSize() - 2);
//    state.push_back(patternPos(0));
//    state.push_back(patternPos(1));
        if (simulationError_) {
            std::vector<double> newstate = currentState;
            for (int i=3; i<5; ++i) {
                newstate[i] = state[i];
            }
            return newstate;
        }

        //TODO: do I need combinations of features?

        return state;
    }

    void takeActionFull(std::vector<double> a) {
//        std::cout << a[0] << " " << a[1] << " " << a[2] << " " << a[3] << " " << a[4] << std::endl;

        Vector3 shift;
        shift << a[0], a[1], a[2];

        //Don't take parts of action if it's invalid
        Vector3 pos = sim->containerPos();
//        if (pos(2) > MAX_HEIGHT && shift(2) > 0) {
//            shift(2) = 0;
//        } else if (pos(2) < MIN_HEIGHT && shift(2) < 0) {
//            shift(2) = 0;
//        }

        float_type dPhi = a[3];
        float_type dTheta = a[4];

//        if (sim->theta() > MAX_THETA && dTheta > 0) {
//            dTheta = 0;
//        } else if (sim->theta() < MIN_THETA && dTheta < 0) {
//            dTheta = 0;
//        }

        sim->integrateContainerPos(shift, dPhi, dTheta);
    }

    void takeActionReduced(std::vector<double> a) const {
//        std::cout << a[0] << " " << a[1] << std::endl;
        Vector3 shift;
        shift << a[0], a[1], 0.0;

        //both technically equivalent, just different values (take care of bounds!!!)
//        sim->updateContainerPos(shift, 0.0, 0.0); // will calculate velocity from shift / dt
        sim->integrateContainerPos(shift, 0.0, 0.0); // will use shift as velocity
    }

    double getRewardAll() {
        //TODO: velocities/positions need to be part of state if I give negative rewards for them! Otherwise, no negative rewards for that.

        float_type invalid_reward = -100.0; //TODO: find good magnitude of reward for invalid states

        if (simulationError_) {
            return invalid_reward;
        }

        //limits on angle
        if (sim->theta() < MIN_THETA || sim->theta() > MAX_THETA) {
            std::cout << "Invalid theta! " << sim->theta() << std::endl;
            return invalid_reward;
        }

        //limits on height
        Vector3 containerPos = sim->containerPos();
        if (containerPos(2) < MIN_HEIGHT || containerPos(2) > MAX_HEIGHT) {
            std::cout << "Invalid height! " << containerPos(2) << std::endl;
            return invalid_reward;
        }

        //limits on velocities
        Vector3 containerVel = sim->containerVel();
        if (std::abs(containerVel(0)) > MAX_VEL || std::abs(containerVel(1)) > MAX_VEL || std::abs(containerVel(2)) > MAX_VEL) {
            std::cout << "Invalid container velocity!" << std::endl;
            return invalid_reward;
        }
        if (std::abs(sim->phiVel()) > MAX_VEL) {
            std::cout << "Invalid phi velocity!" << std::endl;
            return invalid_reward;
        }
        if (std::abs(sim->thetaVel()) > MAX_VEL) {
            std::cout << "Invalid theta velocity!" << std::endl;
            return invalid_reward;
        }

        if (sim->getPatternSize() < 3) {
            return 0.0;
        }


        float_type err = 0.0;

        //Calculate error for all simulated patterns points that have not been looked at.
        //Often, multiple points at once are deposited, so I'm missing out on the error of some if I only consider the last point.

        for (int i=lastIdx_+1; i<=sim->getPatternSize() - 2; ++i) {
            float_type curvature = sim->curvature(i); //curvature of pattern for last point that has two neighbours on floor
            float_type arc_length = sim->arcLength(i) - zero_arc_length_;

            while (patternIdx_ < (int) pattern_.size() - 1 && arc_length > pattern_[patternIdx_ + 1].first) {
                ++patternIdx_;
            }

            float_type x0 = pattern_[patternIdx_].first;
            float_type x1 = pattern_[patternIdx_ + 1].first;

            //linearly interpolate curvature between known points
            float_type goal_curvature = ((x1 - arc_length) * pattern_[patternIdx_].second +
                                         (arc_length - x0) * pattern_[patternIdx_ + 1].second
                                        ) / (x1 - x0);

//        err += std::abs(curvature - goal_curvature); //TODO: try out L2 error instead of L1 error
            float_type e = curvature - goal_curvature;
            err += e*e;
//        std::cout << err << std::endl;
        }

        lastIdx_ = std::max(lastIdx_, sim->getPatternSize() - 2);

//    std::cout << "lastIdx_ = " << lastIdx_ << std::endl;

        return -err;
    }

    double getRewardNormalized() {
        //velocities/positions need to be part of state if I give negative rewards for them! Otherwise, no negative rewards for that.

        float_type invalid_reward = -1.0;

        if (simulationError_) {
            return invalid_reward;
        }

        //limits on angle
        if (sim->theta() < MIN_THETA || sim->theta() > MAX_THETA) {
            std::cout << "Invalid theta! " << sim->theta() << std::endl;
            return invalid_reward;
        }

        //limits on height
        Vector3 containerPos = sim->containerPos();
        if (containerPos(2) < MIN_HEIGHT || containerPos(2) > MAX_HEIGHT) {
            std::cout << "Invalid height! " << containerPos(2) << std::endl;
            return invalid_reward;
        }

        //limits on velocities
//    Vector3 containerVel = sim->containerVel();
//    if (std::abs(containerVel(0)) > MAX_VEL || std::abs(containerVel(1)) > MAX_VEL || std::abs(containerVel(2)) > MAX_VEL) {
//        std::cout << "Invalid container velocity!" << std::endl;
//        return invalid_reward;
//    }
//    if (std::abs(sim->phiVel()) > MAX_VEL) {
//        std::cout << "Invalid phi velocity!" << std::endl;
//        return invalid_reward;
//    }
//    if (std::abs(sim->thetaVel()) > MAX_VEL) {
//        std::cout << "Invalid theta velocity!" << std::endl;
//        return invalid_reward;
//    }

        if (sim->getPatternSize() < 3) {
            return 0.0;
        }


        float_type err = 0.0;

        //Calculate error for all simulated patterns points that have not been looked at.
        //Often, multiple points at once are deposited, so I'm missing out on the error of some if I only consider the last point.
        //-> Maybe this shouldn't actually be the case for final simulation!

        bool noNewPoint = true;
        int patternSize = sim->getPatternSize();

        for (int i=lastIdx_+1; i<=patternSize - 2; ++i) {
            noNewPoint = false;

            float_type curvature = sim->curvature(i); //curvature of pattern for last point that has two neighbours on floor
            float_type arc_length = sim->arcLength(i) - zero_arc_length_;

            while (patternIdx_ < (int) pattern_.size() - 1 && arc_length > pattern_[patternIdx_ + 1].first) {
                ++patternIdx_;
            }

            float_type x0 = pattern_[patternIdx_].first;
            float_type x1 = pattern_[patternIdx_ + 1].first;

            //linearly interpolate curvature between known points
            float_type goal_curvature = ((x1 - arc_length) * pattern_[patternIdx_].second +
                                         (arc_length - x0) * pattern_[patternIdx_ + 1].second
                                        ) / (x1 - x0);

//        err += std::abs(curvature - goal_curvature); //L1 error
            float_type e = curvature - goal_curvature;
            err += e*e; //L2 error
        }

        int lastIdxOld = lastIdx_;
        lastIdx_ = std::max((int)lastIdx_, patternSize - 2);
        float_type totarclength = sim->arcLength(lastIdx_) - sim->arcLength(lastIdxOld);

        float_type maxErr = 100.0; //TODO: value

        std::cout << err << " " << totarclength << std::endl;

        if (noNewPoint) {
            return 0.0;
        } else if (err > maxErr) {
            return 0.0;
        } else {
            return (1.0 - err / maxErr) * totarclength;
        }
    }

    double getRewardPosition() {
        //Compare position at last point with interpolated position of pattern
        int patternSize = sim->getPatternSize();
        lastIdx_ = std::max((int)lastIdx_, patternSize - 2);

        float_type arc_length = sim->arcLength(lastIdx_) - zero_arc_length_;

        while (patternIdx_ < (int) pattern_.size() - 2 && arc_length > pattern_[patternIdx_ + 1].first) {
            ++patternIdx_;
        }

        float_type x0 = pattern_[patternIdx_].first;
        float_type x1 = pattern_[patternIdx_ + 1].first;
//        std::cout << x0 << " " << x1 << " " << arc_length << std::endl;
        Vector2 goal_position = ((x1 - arc_length) * patternPoints_[patternIdx_] +
                                 (arc_length - x0) * patternPoints_[patternIdx_ + 1]
                                ) / (x1 - x0);

        Vector2 position = sim->getPattern(lastIdx_) - sim->getPattern(stabilizedLastIdx);
//        std::cout << goal_position(0) << " " << goal_position(1) << "    " << position(0) << " " << position(1) << std::endl;
        float_type diff = (goal_position - position).norm();

        return -diff;
    }

    double getRewardPositionIntegrated() {
        //Compare position at last point with interpolated position of pattern
        int patternSize = sim->getPatternSize();
        double totalDiff = 0;

        double da = 0.01; //integration step size
        int idx = lastIdx_;
//        std::cout << "patternSize = " << patternSize << ", idx = " << idx << std::endl;
    
        float_type arc_length0 = sim->arcLength(idx) - zero_arc_length_;
        float_type arc_length1 = sim->arcLength(idx+1) - zero_arc_length_;
        float_type arc_length_end = sim->arcLength(patternSize-1) - zero_arc_length_;
        
        double al;
        for (al=arc_length0+da; al<=arc_length_end; al+=da) {

            while (idx < patternSize-2 && al > arc_length1) {
                ++idx;
                arc_length0 = sim->arcLength(idx) - zero_arc_length_;
                arc_length1 = sim->arcLength(idx+1) - zero_arc_length_;
//                std::cout << "idx = " << idx << std::endl;
            }

            while (patternIdx_ < (int) pattern_.size()-2 && al > pattern_[patternIdx_ + 1].first) {
                ++patternIdx_;
            }

            float_type x0 = pattern_[patternIdx_].first;
            float_type x1 = pattern_[patternIdx_ + 1].first;
//            std::cout << x0 << " " << x1 << " " << al << std::endl;
            Vector2 goal_position = ((x1 - al) * patternPoints_[patternIdx_] +
                                     (al - x0) * patternPoints_[patternIdx_ + 1]
                                    ) / (x1 - x0);

        
            Vector2 position0 = sim->getPattern(idx) - sim->getPattern(stabilizedLastIdx);
            Vector2 position1 = sim->getPattern(idx+1) - sim->getPattern(stabilizedLastIdx);

            Vector2 position = ((arc_length1 - al) * position0 +
                                (al - arc_length0) * position1
                               ) / (arc_length1 - arc_length0);

//            std::cout << arc_length0 << " " << arc_length1 << " " << al << std::endl;

            float_type diff = (goal_position - position).norm();
//            std::cout << goal_position(0) << "," << goal_position(1) << " " <<  position(0) << "," << position(1) << " " <<  diff << std::endl;
            totalDiff += diff * da;
        }

        if (al < arc_length_end) {
            //Do last segment (shorter than da)
            
            double dal = arc_length_end - al;
            al = arc_length_end;

            while (patternIdx_ < (int) pattern_.size()-2 && al > pattern_[patternIdx_ + 1].first) {
                ++patternIdx_;
            }

            float_type x0 = pattern_[patternIdx_].first;
            float_type x1 = pattern_[patternIdx_ + 1].first;
//            std::cout << x0 << " " << x1 << " " << al << std::endl;
            Vector2 goal_position = ((x1 - al) * patternPoints_[patternIdx_] +
                                     (al - x0) * patternPoints_[patternIdx_ + 1]
                                    ) / (x1 - x0);

            Vector2 position = sim->getPattern(idx+1) - sim->getPattern(stabilizedLastIdx);

            float_type diff = (goal_position - position).norm();
//            std::cout << goal_position(0) << "," << goal_position(1) << " " <<  position(0) << "," << position(1) << " " <<  diff << std::endl;
            totalDiff += diff * dal;
        }
        
        lastIdx_ = std::min(idx + 1, patternSize - 1);

//        std::cout << totalDiff << std::endl;

        return -totalDiff;
    }
};




int main(int argc, const char *argv[]) {
    //communication:
    const int socket = std::stoi(argv[1]);

    double patternScaleFactor = 1.0;
    if (argc > 3) {
        patternScaleFactor = std::atof(argv[3]);
    }

    int everyNoutput = 0;
    if (argc > 3) {
        everyNoutput = std::atoi(argv[4]);
    }
    bool writePattern = (everyNoutput > 0);

    std::cout << writePattern << " " << everyNoutput << std::endl;

    ViscousThreadsEnvironment env(argv[2], patternScaleFactor, everyNoutput, 0.02, 0.5, (Vector3() << 0.8, 0.0, 0.0).finished());

    const int control_vars = env.actionDimension();
    const int state_vars = env.stateDimension();

    //socket number is given by RL as first argument of execution
    Communicator comm(socket, state_vars, control_vars);

    //OPTIONAL: action bounds
    bool bounded = true;
//    std::vector<double> lower_action_bound{-0.5, -0.5, -0.5, -0.5, -0.5}, upper_action_bound{0.5, 0.5, 0.5, 0.5, 0.5};
//    std::vector<double> lower_action_bound{-0.2, -0.2}, upper_action_bound{0.2, 0.2};
    std::vector<double> lower_action_bound{-1.0, -1.0}, upper_action_bound{1.0, 1.0};
//    std::vector<double> lower_action_bound{0.75, -0.05}, upper_action_bound{0.85, 0.05};
    comm.set_action_scales(upper_action_bound, lower_action_bound, bounded);

    /*
      // ALTERNATIVE for discrete actions:
      vector<int> n_options = vector<int>{2};
      comm.set_action_options(n_options);
      // will receive either 0 or 1, app chooses resulting outcome
    */

    //OPTIONAL: hide state variables.
    //TODO
    // e.g. show cosine/sine but not angle
//    std::vector<bool> b_observable = {true, true, true, false, true, true};
//    comm.set_state_observable(b_observable);

    //OPTIONAL: set space bounds
    //TODO
//    std::vector<double> upper_state_bound{1, 1, 1, 1, 1, 1};
//    std::vector<double> lower_state_bound{-1, -1, -1, -1, -1, -1};
//    comm.set_state_scales(upper_state_bound, lower_state_bound);

    std::cout << "Before train loop" << std::endl;

    while (true) //train loop
    {
        std::cout << "Start episode" << std::endl;

        //reset environment:
        env.reset(comm.gen); //comm contains rng with different seed on each rank

        comm.sendInitState(env.getState()); //send initial state

        while (true) //simulation loop
        {
            std::vector<double> action = comm.recvAction();

            //advance the simulation:
            bool terminated = env.advance(action);

            //TODO: state is invalid when advance ended with error -> need to figure out what state to use if there was en error!
            //Maybe take last state, but replace action with current action?
            std::vector<double> state = env.getState();
            double reward = env.getReward();

            if (terminated)  //tell smarties that this is a terminal state
            {
                comm.sendTermState(state, reward);
                if (writePattern) {
//                    std::cout << "writePattern" << std::endl;
                    env.writePattern();
                }
                break;
            } else comm.sendState(state, reward);
        }
    }
}
